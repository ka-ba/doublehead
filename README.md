# What is DoubleHead?
DoubleHead is a small application to play the card game ["Doppelkopf"](https://en.wikipedia.org/wiki/Doppelkopf) in a distributed (that is: pandemic-compliant) setting over the network.
DoubleHead has no notion of the rules of Doppelkopf (and doesn't need to have). Nonetheless some choice are already build in and are not configurable:

* The game is played with 48 cards. That means **with** "9" cards.
* A hand with more than 4 "9" cards, or a hand with less than 4 trump cards are never dealt. New hands are generated until these conditions are met for all 4 players.
* 4 to 6 players can participate in a session. When there are more than 4, they are joining individual games in a round-robin fashion in a way that each game only has 4 players. This scheme of taking turns is fixed; no more than 6 players can actively join the session; players 7 to x are limited to observing the game.

# How to run DoubleHead
The applications is organised is a client-server fashion. You have to start a server before you start the clients. The clients all connect to one server and use it to handle their inter-client communication.
Since this currently is a source-only distribution, you have to compile it to generate a jar file. (To explain how to do this is outside the scope of this readme!) Copy the resulting jar file to a safe and convenient place at remember this place. Also please note, that the artwork of the cards is not distributed with this repo. To see something useful you'll have to add png files to the src/ressources folder for each card, plus one for the backside (and maybe some others). Fore the filenames to use please see the class Cards.

## System requirements
To run DoubleHead (the server and/or the client) you need to have a JRE (Java(tm) Runtime Environment) installed. The version must be at least Java 8. To compile, you will need a JDK.

## Server
Start a server on the command line (using whatever the shell is called on your favourite operating system). The server takes 2 arguments:
~~~console
java -cp [/path/to/jar/file] me.kaba.doublehead.backend.Server [hostname.domain.tld] [port num]
~~~

Please note that the server must be reachable for all clients on the given hostname / port num combination. In a private distributed setting you may need to configure your WAN router and / or the firewall settings of the server machine. (again: the how is outside the scope of this readme)

## Client
On most modern operating systems with a GUI you should be able to start a client by double-clicking the jar file. Of course you may use the command line as well:
~~~console
java -jar [/path/to/jar/file]
~~~

On first use, or in case the server that was used last time is not reachable, a config dialog can be opened that asks for connection parameters (hostname and port num, cf. above) and a players name. (NB: There is a known issue that a client may mis-behave directly after changing these parameters. You may have to re-start it once in this case.)

If the client can connect to the server it will do so. As soon as at least 4 clients have connected, the first game will start with dealing the hands.

## Alternative client

If you would fancy an Android client instead of the Java client, you may want to have a look at this clever adaptation: https://gitlab.com/wuapps/doublehead/

# Using the client
In general you will not find a complete user manual here. You have to find your way around yourself, but let me list a few hints:

* As the program is limited to a somewhat basic functionality, it is advisable to have a separate voice channel available alongside the game. But you will want to have this anyway to talk to your friends, right? ;)
* The 4 players have to jointly decide on who won the current trick. To instruct the program on the decision, there will be 4 check-boxes above the cards of the current trick as soon as it is complete. The program will wait for this decision to be taken.
* If a client gets disconnected, it can re-connect. That should work seamlessly. However, when a players permanently leaves the session whiles she was actively participating in a game, you will be stuck. The program will wait for this player to play her next card. You will have to restart all components and start a new session to continue.
* In a session with 5 or 6 players, a player **can** leave the session while he is not actively participating in the current game. He will not be considered for future games. (unless he re-connects some time later to join in anew)
* You can not undo playing a card. Whenever a player played a wrong card, you will have to agree how to deal with the situation.
* You **can** re-assign your vote in the phase of deciding the winner of a trick **until** the decision is jointly taken: just choose another check-box.

Always remember: the program doesn't know anything about the rules of the game (besides that you don't play with more than 4 "9" cards or less than 4 trump cards) and it will behave that way!