/*
 * Copyright (C) 2020-2021 kaba
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.kaba.doublehead.swing;

import java.awt.Color;
import me.kaba.doublehead.middleware.Cards;
import me.kaba.doublehead.middleware.Savvy;
import me.kaba.doublehead.middleware.Session;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import javax.swing.AbstractButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JToggleButton;
import java.awt.Font;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.ImageIcon;
import me.kaba.doublehead.middleware.PlayerMgr;
import me.kaba.doublehead.middleware.VersionInfo;

/**
 *
 * @author kaba
 */
class MainFrame extends javax.swing.JFrame implements Savvy{

    public final static String CPK_CARD="card";
    public final static String CPK_PLAYER="player";

    /**
     * Creates new form MainFrame
     */
    private MainFrame( String name_override ) {
        initComponents();
        partyP = new ImagePanel( "party.jpg" );
        getContentPane().add( partyP ).setVisible( false );
        handSP.getViewport().setOpaque( false );
        gameSP.getViewport().setOpaque( false );
        overlayJP.setVisible( false );
        handSP.setVisible( false );
        gameSP.setVisible( false );
        jbPlayCard.setVisible( false );
        jbPlayCard.setEnabled( false );
        lPausing.setVisible( false );
        sortP.setVisible( false );
        sortCB.setEnabled( false );
        cardButtons = Arrays.asList( new JToggleButton[]{ handTB1, handTB2, handTB3, handTB4, handTB5, handTB6, handTB7, handTB8, handTB9, handTB10, handTB11, handTB12 } );
        receiveButtons = Arrays.asList( new JRadioButton[]{ p1ReceiverRB, p2ReceiverRB, p3ReceiverRB, p4ReceiverRB });
        currentLabels = Arrays.asList( new JLabel[]{ p1CurrentL, p2CurrentL, p3CurrentL, p4CurrentL });
        recentLabels = Arrays.asList( new JLabel[]{ p1REcentL, p2RecentL, p3RecentL, p4RecentL });
        tricksLabels = Arrays.asList( new JLabel[]{ p1TricksL, p2TricksL, p3TricksL, p4TricksL });
        versionL.setText( VersionInfo.getInstance().toString() );
        URL url = getClass().getResource("/ressources/bounce.gif");
        jubilIcon = new ImageIcon(url);
        url = getClass().getResource("/ressources/troete.gif");
        partyIcon = new ImageIcon(url);
        bd = BD.today();
        loadProperties( name_override );
        if( PreConnectDialog.propsComplete(props) )
            connect();
    }

    private final List<JToggleButton> cardButtons;
    private final List<JRadioButton> receiveButtons;
    private final List<JLabel> currentLabels, recentLabels, tricksLabels;
    private final List<Cards.Card> backsides = Arrays.asList( new Cards.Card[]{ Cards.getInstance().back, Cards.getInstance().back, Cards.getInstance().back, Cards.getInstance().back } );
    private final List<String> dots = Arrays.asList( new String[]{ ".",".",".","." } );

    void enableJBConnect() {
        jbConnect.setEnabled( true );
    }
    
    void connect() {
        System.out.println( "connecting for "+ props.getProperty(PK_USERNAME) );
        ses = new Session( this, props );
        ses.go();
    }

    @Override
    public void disconnect( String reason ) {
//        if( ses == null ) {
//            System.err.println( "MainFrame. disconnect: without session ignoring request with: "+reason );
//            return;
//        }
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                if( ses != null )
                    ses.end();
                ses = null;
                enableJBConnect();
                System.out.println( "MainFrame.disconnect: dropped session because: "+reason );
            }
        });
    }

    @Override
    public void setupGameView( List<String> orders ) {
        assert ( ! orders.isEmpty() );
        Set<String> oset = new HashSet<>( orders );
        assert oset.size() == orders.size();  // no dupes allowed
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                jbConnect.setEnabled( false );
                jbConnect.setVisible(false );
                handSP.setVisible( true );
                handSP.setEnabled( true );
                gameSP.setVisible( true );
                gameSP.setEnabled( true );
                playersP.setVisible( true );
                jbPlayCard.setVisible( true );
                jbPlayCard.setEnabled( false );
                lPausing.setVisible( false );
                showBacksidesButtons();
                showCardsLabels( currentLabels, backsides.subList(0,4), dots.subList(0,4), true );
                showCardsLabels( recentLabels, backsides.subList(0,4), dots.subList(0,4), true );
                showCardsLabels( tricksLabels, backsides.subList(0,4), dots.subList(0,4), true );
                envisTrickVotersPriv( false, false );
                sortCB.removeAllItems();
                for( String o : orders )
                    sortCB.addItem( o );
            }
        });
    }
    
    private void showBacksidesButtons() {
        for( JToggleButton b : cardButtons )
            showCardButton( b, Cards.getInstance().back );
//        cardButtons.get(2).setEnabled( true );
    }

    /** &gt;0 true, ==0 false, &lt;0 don't change */
    private void visenCardButtons( int vis, int ena ) {
        for( JToggleButton b : cardButtons ) {
            if( vis >= 0 )
                b.setVisible( (vis==0?false:true) );
            if( ena >= 0 )
                b.setEnabled((ena==0?false:true) );
        }
    }

    private void showCardButton( JToggleButton b, Cards.Card c ) {
        CardView.Card cv = CardView.getInstance().getCardViewCardToCardsCard( c );
        assert (cv.icon==null) == (cv.iconSel==null);
        b.setIcon( cv.icon );
        b.setDisabledIcon( cv.icon );
        b.setSelectedIcon( cv.iconSel );
        if( cv.icon != null )
            b.setText( null );
        else
            b.setText( c.toolTip );
        b.setToolTipText( c.toolTip );
        b.putClientProperty( CPK_CARD, c );
    }

    private void showCardsLabels( List<JLabel> labels, List<Cards.Card> cards, List<PlayerMgr.Player> spieler ) {
        assert labels.size() == cards.size();
        assert labels.size() == spieler.size();
        for( int i=0; i< labels.size(); i++ )
            showCardLabel(labels.get(i), cards.get(i), spieler.get(i) );
    }
    private void showCardsLabels( List<JLabel> labels, List<Cards.Card> cards, List<String> texts, boolean b ) {
        assert labels.size() == cards.size();
        assert labels.size() == texts.size();
        for( int i=0; i< labels.size(); i++ )
            showCardLabel( labels.get(i), cards.get(i), texts.get(i) );
    }

    private void showCardLabel( JLabel l, Cards.Card c, PlayerMgr.Player p ) {
        CardView.Card cv = CardView.getInstance().getCardViewCardToCardsCard( c );
        l.setIcon( cv.icon );
        l.setToolTipText( c.toolTip );
        colourise (l, p, Font.BOLD );
        l.setText( p.name );
        l.putClientProperty( CPK_CARD, c );
    }
    private void showCardLabel( JLabel l, Cards.Card c, String s ) {
        CardView.Card cv = CardView.getInstance().getCardViewCardToCardsCard( c );
        l.setIcon( cv.icon );
        l.setToolTipText( c.toolTip );
        l.setText( s );
        l.putClientProperty( CPK_CARD, c );
    }

    private void colourise (JLabel l, PlayerMgr.Player p, int b) {
        l.setForeground( new Color(0,128,0) );
        Font f = l.getFont();
        Font bf = f.deriveFont(Font.PLAIN);
        l.setFont (bf);
        if( p.me ) {
            l.setForeground( Color.blue );
            f = l.getFont();
            bf = f.deriveFont(b);
            l.setFont (bf);
        }
        if( ! p.getOnline() )
            l.setForeground( Color.gray );
    }
    
    @Override
    public void startObserving() {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                showBacksidesButtons();
                visenCardButtons( 1, 0 );
                jbPlayCard.setVisible( false );
                envisTrickVotersPriv( false, false );
                lPausing.setVisible( true );
            }
        });
    }

    @Override
    public void stopObserving() {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                visenCardButtons( 1, -1 );
                lPausing.setVisible( false );
            }
        });
    }

    @Override
    public void showPlayers( List<PlayerMgr.Player> players, int max ) {
        java.awt.EventQueue.invokeLater( new Runnable() {
            @Override
            public void run() {
                List<JLabel> comps = new ArrayList<>( players.size()+1 );
                int i = 0;
                for( PlayerMgr.Player p : players ) {
                    JLabel l = new JLabel( p.name );
                    colourise (l, p, Font.PLAIN);
                    if( i >= max )
                        l.setForeground( new Color(0,128,128) );
                    l.setOpaque( true );
                    comps.add( l );
                    if( ++i == max ) {
                        JLabel s = new JLabel( "|" );
                        s.setForeground( Color.red );
                        comps.add( s );
                    }
                }
                playersP.removeAll();
                for( JComponent c : comps )
                    playersP.add( c );
                playersP.validate();
            }
        });
    }

    @Override
    public void showNewHand( List<Cards.Card> hand ) {
        assert hand.size() == 12;
        assert cardButtons.size() == 12;
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                showHandSorted( hand );
                visenCardButtons( 1, 0 );
                sortCB.setEnabled( true );
                sortP.setVisible( true );
            }
        });
    }

    @Override
    public void removePlayed( Cards.Card card ) {
        assert card != null;
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                JToggleButton cb=null, cit = cardInTransit; // protect against race condition
                cardInTransit = null;
                if( cit != null ) {
                    assert card.equals( cit.getClientProperty(CPK_CARD) );
                    assert cit.getClientProperty(CPK_CARD).equals( card );
                    cb = cit;
                } else
                    for( JToggleButton t : cardButtons ) {
                        System.out.println( "looking for "+card.handle+" in: "+t.getClientProperty(CPK_CARD) );
                        if( t.isVisible() && card.equals((Cards.Card)(t.getClientProperty(CPK_CARD))) ) {
                            cb = t;
                            break;
                        }
                    }
                assert cb != null;
                if( cb == null )
                    throw new IllegalArgumentException( "cannot find card to remove from display: "+ card );
                cb.setVisible( false );
                cb.setSelected( false );
                sortCB.setEnabled( false );
                sortP.setVisible( false );
            }
        });
    }

    /** to be called from inside GUI thread only! */
    private void showHandSorted( List<Cards.Card> hand ) {
        assert hand.size() == 12;
        assert cardButtons.size() == 12;
        Cards.getInstance().sort( hand, (String)(sortCB.getSelectedItem()) );
        for( int i=0; i<12; i++ )
            showCardButton(cardButtons.get(i), hand.get(i) );
    }
    
    @Override
    public void showCurrent( List<PlayerMgr.Player> players, List<Cards.Card> cards ) {
        assert players.size() == currentLabels.size();
        assert cards.size() == currentLabels.size();
        assert players.size() == receiveButtons.size();
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                showCardsLabels( currentLabels, cards, players );
                for( JLabel l : currentLabels ) {
                    l.setEnabled( true );
                    l.setVisible( true );
                }
                for( int i=0; i<players.size(); i++ )
                    receiveButtons.get(i).putClientProperty( CPK_PLAYER, players.get(i) );
            }
        });
    }

    @Override
    public void showRecent( List<PlayerMgr.Player> players, List<Cards.Card> cards ) {
        assert players.size() == recentLabels.size();
        assert cards.size() == recentLabels.size();
        boolean party_poo=false, me_poo=false;
        for( PlayerMgr.Player p : players )
            if( bd.isKid(p.name) ) {
                party_poo = true;
                if( p.me )
                    me_poo = true;
            }
        final boolean party=party_poo, me=me_poo&(!partyP.isVisible());
        System.out.println( "" + (party?"having":"no") + " party" + (me?" for me":"") );
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                showCardsLabels( recentLabels, cards, players );
                if( party )
                    partyP.setVisible( true );
                if( me ) {
                    overayJL.setVisible( true );
                    overlayJP.setVisible( true );
                    overayJL.setIcon( partyIcon );
                }
            }
        });
        if( me ) {
            try {
                Thread.sleep( 5500L );
            } catch( InterruptedException ex ) {}
            java.awt.EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                        overayJL.setIcon( null );
                        overlayJP.setVisible( false );
                }
            });
        }
    }

    @Override
    public void showTricks( List<PlayerMgr.Player> players, List<Integer> tricks ) {
        assert players.size() == tricksLabels.size();
        assert tricks.size() == tricksLabels.size();
        List<String> s = new ArrayList<>( tricks.size() );
        for( int i=0; i<tricks.size(); i++ )
            s.add( players.get(i).name+": "+tricks.get(i) );
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                showCardsLabels( tricksLabels, backsides.subList(0,4), s, true );
            }
        });
    }

    @Override
    public void jubilate( List<PlayerMgr.Player> players ){
        for( int i=0; i<players.size(); i++ ) {
            if( players.get(i).me ) {
                JLabel label = tricksLabels.get(i);
                java.awt.EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        label.setIcon( jubilIcon );
                    }
                });
                jubilTask = new TimerTask (){
                    @Override
                    public void run (){
                        java.awt.EventQueue.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                CardView.Card cv = CardView.getInstance().getCardViewCardToCardsCard( backsides.get(0) );
                                label.setIcon( cv.icon);
                            }
                        });
                    }
                };
                jubilTimer =new Timer ();
                jubilTimer.schedule(jubilTask, 3000L);
                // ignore that old Timer might still be running
                break;
            }
        }
    }

    @Override
    public void enablePlayCard( boolean e ) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                enableCardButtons( e );
            }
        });
    }

    private void enableCardButtons( boolean bo ) {
        visenCardButtons( -1, (bo?1:0) );
        handBG.clearSelection(); // FIXME: OK here in this general manner?
    }

    @Override
    public void envisTrickVoters( boolean visible, boolean enable ) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                envisTrickVotersPriv(visible, enable);
            }
        });
    }
    
    private void envisTrickVotersPriv( boolean visible, boolean enable) {
        for( JRadioButton b : receiveButtons ) {
//            b.setSelected( false );
            b.setVisible( visible );
            b.setEnabled( enable );
        }
        currentBG.clearSelection();
    }

    @Override
    public void showMessageDialog( Object message, String title, int messageType ) {
        javax.swing.JOptionPane.showMessageDialog( this, message, title, messageType );
    }

    @Override
    public void showTricksDialog( List<List<Cards.Card>> tricks ) {
        TricksDialog.show( this, tricks );
    }

    private <T extends AbstractButton> T whichSelected( Collection<T> comps ) {
        for( T c : comps )
            if( c.isSelected() )
                return c;
        return null;
    }

    private void removeEmpty( Properties p ) {
        for( Iterator i=p.keySet().iterator(); i.hasNext();  )
            if( p.getProperty((String)(i.next())).isEmpty() )
                i.remove();
    }

    private void loadProperties( String name_override ) {
        Properties p = new Properties();
        p.setProperty( PK_PORT, "1709" ); // default
        try( InputStream in = new FileInputStream(System.getProperty("user.home")+File.separator+"DoubleHead.properties") ) {
            p.load( in );
            removeEmpty( p );
            System.err.println( "Properties read: "+p+" ("+name_override+")" );
        } catch( IOException e ) { /*ignore*/ }
        if( (name_override!=null) && (!name_override.isEmpty()) ) {
            p.setProperty( PK_USERNAME, name_override );
        }
        props = p;
    }

    void storeProperties() {
        try( OutputStream out = new FileOutputStream(System.getProperty("user.home")+File.separator+"DoubleHead.properties") ) {
            removeEmpty( props );
            props.setProperty( PK_USERNAME, props.getProperty(PK_USERNAME).replaceAll("[^a-zA-Z]","") );
            props.store( out, null );
            System.err.println( "Properties stored: "+props );
        } catch( IOException e ) {
            System.err.println( "failed to store Properties to "+System.getProperty("user.home")+File.separator+"DoubleHead.properties"+"\n"+props+"\n"+e );
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        handBG = new javax.swing.ButtonGroup();
        currentBG = new javax.swing.ButtonGroup();
        overlayJP = new javax.swing.JPanel();
        overayJL = new javax.swing.JLabel();
        contentsJP = new javax.swing.JPanel();
        sortP = new javax.swing.JPanel();
        sortL = new javax.swing.JLabel();
        sortCB = new javax.swing.JComboBox<>();
        handSP = new javax.swing.JScrollPane();
        handP = new javax.swing.JPanel();
        handTB1 = new javax.swing.JToggleButton();
        handTB2 = new javax.swing.JToggleButton();
        handTB3 = new javax.swing.JToggleButton();
        handTB4 = new javax.swing.JToggleButton();
        handTB5 = new javax.swing.JToggleButton();
        handTB6 = new javax.swing.JToggleButton();
        handTB7 = new javax.swing.JToggleButton();
        handTB8 = new javax.swing.JToggleButton();
        handTB9 = new javax.swing.JToggleButton();
        handTB10 = new javax.swing.JToggleButton();
        handTB11 = new javax.swing.JToggleButton();
        handTB12 = new javax.swing.JToggleButton();
        gameSP = new javax.swing.JScrollPane();
        gameP = new javax.swing.JPanel();
        currentP = new javax.swing.JPanel();
        p1CurrentP = new javax.swing.JPanel();
        p1ReceiverRB = new javax.swing.JRadioButton();
        p1CurrentL = new javax.swing.JLabel();
        p2CurrentP = new javax.swing.JPanel();
        p2ReceiverRB = new javax.swing.JRadioButton();
        p2CurrentL = new javax.swing.JLabel();
        p3CurrentP = new javax.swing.JPanel();
        p3ReceiverRB = new javax.swing.JRadioButton();
        p3CurrentL = new javax.swing.JLabel();
        p4CurrentP = new javax.swing.JPanel();
        p4ReceiverRB = new javax.swing.JRadioButton();
        p4CurrentL = new javax.swing.JLabel();
        recentP = new javax.swing.JPanel();
        p1REcentL = new javax.swing.JLabel();
        p2RecentL = new javax.swing.JLabel();
        p3RecentL = new javax.swing.JLabel();
        p4RecentL = new javax.swing.JLabel();
        tricksP = new javax.swing.JPanel();
        p1TricksL = new javax.swing.JLabel();
        p2TricksL = new javax.swing.JLabel();
        p3TricksL = new javax.swing.JLabel();
        p4TricksL = new javax.swing.JLabel();
        playersP = new javax.swing.JPanel();
        cmdP = new javax.swing.JPanel();
        jbConnect = new javax.swing.JButton();
        jbPlayCard = new javax.swing.JButton();
        lPausing = new javax.swing.JLabel();
        versionP = new javax.swing.JPanel();
        versionL = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(1300, 400));
        getContentPane().setLayout(new javax.swing.OverlayLayout(getContentPane()));

        overlayJP.setLayout(new java.awt.BorderLayout());

        overayJL.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        overayJL.setOpaque(true);
        overlayJP.add(overayJL, java.awt.BorderLayout.CENTER);

        getContentPane().add(overlayJP);

        contentsJP.setOpaque(false);
        contentsJP.setLayout(new javax.swing.BoxLayout(contentsJP, javax.swing.BoxLayout.Y_AXIS));

        sortP.setOpaque(false);

        sortL.setText("sort order:");
        sortP.add(sortL);

        sortCB.setEnabled(false);
        sortCB.setName(""); // NOI18N
        sortCB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sortCBActionPerformed(evt);
            }
        });
        sortP.add(sortCB);

        contentsJP.add(sortP);

        handSP.setEnabled(false);
        handSP.setMinimumSize(new java.awt.Dimension(100, 50));
        handSP.setOpaque(false);

        handP.setOpaque(false);

        handBG.add(handTB1);
        handTB1.setText("back");
        handTB1.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        handTB1.setEnabled(false);
        handTB1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                handTBActionPerformed(evt);
            }
        });
        handP.add(handTB1);

        handBG.add(handTB2);
        handTB2.setText("back");
        handTB2.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        handTB2.setEnabled(false);
        handTB2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                handTBActionPerformed(evt);
            }
        });
        handP.add(handTB2);

        handBG.add(handTB3);
        handTB3.setText("back");
        handTB3.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        handTB3.setEnabled(false);
        handTB3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                handTBActionPerformed(evt);
            }
        });
        handP.add(handTB3);

        handBG.add(handTB4);
        handTB4.setText("back");
        handTB4.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        handTB4.setEnabled(false);
        handTB4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                handTBActionPerformed(evt);
            }
        });
        handP.add(handTB4);

        handBG.add(handTB5);
        handTB5.setText("back");
        handTB5.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        handTB5.setEnabled(false);
        handTB5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                handTBActionPerformed(evt);
            }
        });
        handP.add(handTB5);

        handBG.add(handTB6);
        handTB6.setText("back");
        handTB6.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        handTB6.setEnabled(false);
        handTB6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                handTBActionPerformed(evt);
            }
        });
        handP.add(handTB6);

        handBG.add(handTB7);
        handTB7.setText("back");
        handTB7.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        handTB7.setEnabled(false);
        handTB7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                handTBActionPerformed(evt);
            }
        });
        handP.add(handTB7);

        handBG.add(handTB8);
        handTB8.setText("back");
        handTB8.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        handTB8.setEnabled(false);
        handTB8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                handTBActionPerformed(evt);
            }
        });
        handP.add(handTB8);

        handBG.add(handTB9);
        handTB9.setText("back");
        handTB9.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        handTB9.setEnabled(false);
        handTB9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                handTBActionPerformed(evt);
            }
        });
        handP.add(handTB9);

        handBG.add(handTB10);
        handTB10.setText("back");
        handTB10.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        handTB10.setEnabled(false);
        handTB10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                handTBActionPerformed(evt);
            }
        });
        handP.add(handTB10);

        handBG.add(handTB11);
        handTB11.setText("back");
        handTB11.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        handTB11.setEnabled(false);
        handTB11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                handTBActionPerformed(evt);
            }
        });
        handP.add(handTB11);

        handBG.add(handTB12);
        handTB12.setText("back");
        handTB12.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        handTB12.setEnabled(false);
        handTB12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                handTBActionPerformed(evt);
            }
        });
        handP.add(handTB12);

        handSP.setViewportView(handP);

        contentsJP.add(handSP);

        gameSP.setOpaque(false);

        gameP.setOpaque(false);
        gameP.setLayout(new javax.swing.BoxLayout(gameP, javax.swing.BoxLayout.X_AXIS));

        currentP.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        currentP.setOpaque(false);

        p1CurrentP.setOpaque(false);
        p1CurrentP.setLayout(new javax.swing.BoxLayout(p1CurrentP, javax.swing.BoxLayout.Y_AXIS));

        currentBG.add(p1ReceiverRB);
        p1ReceiverRB.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        p1ReceiverRB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                receiverRBActionPerformed(evt);
            }
        });
        p1CurrentP.add(p1ReceiverRB);

        p1CurrentL.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        p1CurrentL.setText(".");
        p1CurrentL.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        p1CurrentL.setOpaque(true);
        p1CurrentL.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        p1CurrentP.add(p1CurrentL);

        currentP.add(p1CurrentP);

        p2CurrentP.setOpaque(false);
        p2CurrentP.setLayout(new javax.swing.BoxLayout(p2CurrentP, javax.swing.BoxLayout.Y_AXIS));

        currentBG.add(p2ReceiverRB);
        p2ReceiverRB.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        p2ReceiverRB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                receiverRBActionPerformed(evt);
            }
        });
        p2CurrentP.add(p2ReceiverRB);

        p2CurrentL.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        p2CurrentL.setText(".");
        p2CurrentL.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        p2CurrentL.setOpaque(true);
        p2CurrentL.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        p2CurrentP.add(p2CurrentL);

        currentP.add(p2CurrentP);

        p3CurrentP.setOpaque(false);
        p3CurrentP.setLayout(new javax.swing.BoxLayout(p3CurrentP, javax.swing.BoxLayout.Y_AXIS));

        currentBG.add(p3ReceiverRB);
        p3ReceiverRB.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        p3ReceiverRB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                receiverRBActionPerformed(evt);
            }
        });
        p3CurrentP.add(p3ReceiverRB);

        p3CurrentL.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        p3CurrentL.setText(".");
        p3CurrentL.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        p3CurrentL.setOpaque(true);
        p3CurrentL.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        p3CurrentP.add(p3CurrentL);

        currentP.add(p3CurrentP);

        p4CurrentP.setOpaque(false);
        p4CurrentP.setLayout(new javax.swing.BoxLayout(p4CurrentP, javax.swing.BoxLayout.Y_AXIS));

        currentBG.add(p4ReceiverRB);
        p4ReceiverRB.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        p4ReceiverRB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                receiverRBActionPerformed(evt);
            }
        });
        p4CurrentP.add(p4ReceiverRB);

        p4CurrentL.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        p4CurrentL.setText(".");
        p4CurrentL.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        p4CurrentL.setOpaque(true);
        p4CurrentL.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        p4CurrentP.add(p4CurrentL);

        currentP.add(p4CurrentP);

        gameP.add(currentP);

        recentP.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        recentP.setOpaque(false);

        p1REcentL.setText(".");
        p1REcentL.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        p1REcentL.setOpaque(true);
        p1REcentL.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        recentP.add(p1REcentL);

        p2RecentL.setText(".");
        p2RecentL.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        p2RecentL.setOpaque(true);
        p2RecentL.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        recentP.add(p2RecentL);

        p3RecentL.setText(".");
        p3RecentL.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        p3RecentL.setOpaque(true);
        p3RecentL.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        recentP.add(p3RecentL);

        p4RecentL.setText(".");
        p4RecentL.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        p4RecentL.setOpaque(true);
        p4RecentL.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        recentP.add(p4RecentL);

        gameP.add(recentP);

        tricksP.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        tricksP.setOpaque(false);
        java.awt.FlowLayout flowLayout1 = new java.awt.FlowLayout();
        flowLayout1.setAlignOnBaseline(true);
        tricksP.setLayout(flowLayout1);

        p1TricksL.setText(".");
        p1TricksL.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        p1TricksL.setOpaque(true);
        p1TricksL.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        tricksP.add(p1TricksL);

        p2TricksL.setText(".");
        p2TricksL.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        p2TricksL.setOpaque(true);
        p2TricksL.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        tricksP.add(p2TricksL);

        p3TricksL.setText(".");
        p3TricksL.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        p3TricksL.setOpaque(true);
        p3TricksL.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        tricksP.add(p3TricksL);

        p4TricksL.setText(".");
        p4TricksL.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        p4TricksL.setOpaque(true);
        p4TricksL.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        p4TricksL.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                p4TricksLMousePressed(evt);
            }
        });
        tricksP.add(p4TricksL);

        gameP.add(tricksP);

        gameSP.setViewportView(gameP);

        contentsJP.add(gameSP);

        playersP.setMaximumSize(new java.awt.Dimension(32767, 30));
        playersP.setMinimumSize(new java.awt.Dimension(0, 30));
        playersP.setOpaque(false);
        playersP.setPreferredSize(new java.awt.Dimension(400, 30));
        playersP.setLayout(new java.awt.FlowLayout(0, 9, 9));
        contentsJP.add(playersP);

        cmdP.setMaximumSize(new java.awt.Dimension(32767, 30));
        cmdP.setMinimumSize(new java.awt.Dimension(0, 30));
        cmdP.setOpaque(false);
        cmdP.setPreferredSize(new java.awt.Dimension(400, 30));

        jbConnect.setText("connect");
        jbConnect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbConnectActionPerformed(evt);
            }
        });
        cmdP.add(jbConnect);

        jbPlayCard.setText("play card");
        jbPlayCard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbPlayCardActionPerformed(evt);
            }
        });
        cmdP.add(jbPlayCard);

        lPausing.setText("pausing ...");
        cmdP.add(lPausing);

        contentsJP.add(cmdP);

        versionP.setMinimumSize(new java.awt.Dimension(1, 1));
        versionP.setOpaque(false);
        versionP.setPreferredSize(new java.awt.Dimension(2, 2));
        versionP.setLayout(new java.awt.FlowLayout(2));

        versionL.setFont(new java.awt.Font("Ubuntu", 0, 10)); // NOI18N
        versionL.setForeground(java.awt.Color.gray);
        versionL.setText("xx.xx.xx");
        versionP.add(versionL);

        contentsJP.add(versionP);

        getContentPane().add(contentsJP);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jbConnectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbConnectActionPerformed
        jbConnect.setEnabled( false );
        MainFrame bla = this;
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new PreConnectDialog( bla, true, props ).setVisible( true );
            }
        });
    }//GEN-LAST:event_jbConnectActionPerformed

    private void handTBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_handTBActionPerformed
            jbPlayCard.setVisible( true );
            jbPlayCard.setEnabled( whichSelected(cardButtons) != null );
            cmdP.validate();
    }//GEN-LAST:event_handTBActionPerformed

    private void jbPlayCardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbPlayCardActionPerformed
        JToggleButton selB = whichSelected( cardButtons );
        enableCardButtons( false );
        jbPlayCard.setEnabled( false );
        sortCB.setEnabled( false );
        sortP.setVisible( false );
        Cards.Card selC = (Cards.Card)selB.getClientProperty( CPK_CARD );
        assert cardInTransit == null;
        cardInTransit = selB;
        ses.playCard( selC );
    }//GEN-LAST:event_jbPlayCardActionPerformed

    private void receiverRBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_receiverRBActionPerformed
        JRadioButton selB = whichSelected( receiveButtons );
        ses.voteTrick( ((PlayerMgr.Player)(selB.getClientProperty(CPK_PLAYER))).name );
    }//GEN-LAST:event_receiverRBActionPerformed

    private void p4TricksLMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_p4TricksLMousePressed
        ses.ping();
    }//GEN-LAST:event_p4TricksLMousePressed

    private void sortCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sortCBActionPerformed
        List<Cards.Card> hand = new ArrayList<>( 12 );
        for( JToggleButton b : cardButtons ) {
            Cards.Card c = (Cards.Card)(b.getClientProperty(CPK_CARD));
            if( c.equals(Cards.getInstance().back) )
                return;   // back out on a single backside
            if( c != null )
                hand.add( c );
        }
        assert hand.size() == 12;
        showHandSorted( hand );
    }//GEN-LAST:event_sortCBActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        assert args.length < 2;
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                MainFrame mf = new MainFrame( args.length==0 ? null : args[0] );
                mf.setVisible(true);
            }
        });
    }

    private Properties props;
    private Session ses;
    private JToggleButton cardInTransit=null;
    private final ImageIcon jubilIcon;
    private Timer jubilTimer=null;
    private TimerTask jubilTask=null;
    private final ImagePanel partyP;
    private final ImageIcon partyIcon;
    private final BD bd;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel cmdP;
    private javax.swing.JPanel contentsJP;
    private javax.swing.ButtonGroup currentBG;
    private javax.swing.JPanel currentP;
    private javax.swing.JPanel gameP;
    private javax.swing.JScrollPane gameSP;
    private javax.swing.ButtonGroup handBG;
    private javax.swing.JPanel handP;
    private javax.swing.JScrollPane handSP;
    private javax.swing.JToggleButton handTB1;
    private javax.swing.JToggleButton handTB10;
    private javax.swing.JToggleButton handTB11;
    private javax.swing.JToggleButton handTB12;
    private javax.swing.JToggleButton handTB2;
    private javax.swing.JToggleButton handTB3;
    private javax.swing.JToggleButton handTB4;
    private javax.swing.JToggleButton handTB5;
    private javax.swing.JToggleButton handTB6;
    private javax.swing.JToggleButton handTB7;
    private javax.swing.JToggleButton handTB8;
    private javax.swing.JToggleButton handTB9;
    private javax.swing.JButton jbConnect;
    private javax.swing.JButton jbPlayCard;
    private javax.swing.JLabel lPausing;
    private javax.swing.JLabel overayJL;
    private javax.swing.JPanel overlayJP;
    private javax.swing.JLabel p1CurrentL;
    private javax.swing.JPanel p1CurrentP;
    private javax.swing.JLabel p1REcentL;
    private javax.swing.JRadioButton p1ReceiverRB;
    private javax.swing.JLabel p1TricksL;
    private javax.swing.JLabel p2CurrentL;
    private javax.swing.JPanel p2CurrentP;
    private javax.swing.JRadioButton p2ReceiverRB;
    private javax.swing.JLabel p2RecentL;
    private javax.swing.JLabel p2TricksL;
    private javax.swing.JLabel p3CurrentL;
    private javax.swing.JPanel p3CurrentP;
    private javax.swing.JRadioButton p3ReceiverRB;
    private javax.swing.JLabel p3RecentL;
    private javax.swing.JLabel p3TricksL;
    private javax.swing.JLabel p4CurrentL;
    private javax.swing.JPanel p4CurrentP;
    private javax.swing.JRadioButton p4ReceiverRB;
    private javax.swing.JLabel p4RecentL;
    private javax.swing.JLabel p4TricksL;
    private javax.swing.JPanel playersP;
    private javax.swing.JPanel recentP;
    private javax.swing.JComboBox<String> sortCB;
    private javax.swing.JLabel sortL;
    private javax.swing.JPanel sortP;
    private javax.swing.JPanel tricksP;
    private javax.swing.JLabel versionL;
    private javax.swing.JPanel versionP;
    // End of variables declaration//GEN-END:variables
}
