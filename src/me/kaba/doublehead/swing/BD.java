/*
 * Copyright (C) 2021 kaba
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.kaba.doublehead.swing;

import java.time.MonthDay;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kaba
 */
public class BD {
    public static BD today() {
        assert bdNa.length == bdMo.length;
        assert bdNa.length == bdDa.length;
        MonthDay now = MonthDay.now();
        List<String> newkids = new ArrayList<>( bdNa.length );
        for( int i=0; i<bdNa.length; i++ )
            if( (bdMo[i]==now.getMonthValue()) && (bdDa[i]==now.getDayOfMonth()) )
                newkids.add( bdNa[i] );
        return new BD( newkids );
    }

    public boolean isKid( String name ) {
        for( String k : kids )
            if( name.toLowerCase().contains( k ))
                return true;
        return false;
    }

    private BD( List<String> kids ) {
        this.kids = kids;
    }

    private List<String> kids=null;
    private static final String[] bdNa = { "tigerent", "imspiel", "glck", "silke", "nessi", "kai", "marc" };
    private static final int[] bdMo = { 3, 3, 3, 7, 9, 6, 11 };
    private static final int[] bdDa = { 29, 23, 23, 17, 17, 29, 7 };    
}
