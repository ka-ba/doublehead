/*
 * Copyright (C) 2021 kaba
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.kaba.doublehead.swing;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 *
 * @author kaba
 */
public class ImagePanel extends JPanel {
    public ImagePanel( String fn ) {
        image = getRessourceImage( this.getClass(), fn );
    }

    @Override
    protected void paintComponent( Graphics g ) {
        super.paintComponent( g );
        g.drawImage( image, 0, 0, getWidth(), getHeight(), null );
    }

    private BufferedImage image = null;
    
    public static BufferedImage getRessourceImage( Class base, String fn ) {
        BufferedImage img = new BufferedImage( 10, 10, BufferedImage.TYPE_BYTE_GRAY ); // dummy to return when load fails
        InputStream in = null;
        try {
            in = base.getResourceAsStream( "/ressources/" + fn );
            if (in != null) {
                img = ImageIO.read(in);
            System.out.println( "returning " + fn + " length "+img.getHeight()*img.getHeight() );
        } else {
                System.err.println( "couldn't read image at /ressources/" + fn );
            }
        } catch (IOException e) {
            System.err.println( "exception reading image at  /ressources/" + fn + ": " + e );
        } finally {
            if( in != null )
                try {
                    in.close();
                } catch (IOException ex) { /*ignore this one*/ }
        }
        return img;
    }
}
