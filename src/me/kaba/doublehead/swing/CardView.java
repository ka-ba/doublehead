/*
 * Copyright (C) 2021 ute
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.kaba.doublehead.swing;

import me.kaba.doublehead.middleware.Cards;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

class CardView {

    //Siglegton to have simple access -- as with static methods, but getClass() must be called from non static context
    private static CardView instance = null;

    private final Map<String, CardView.Card> cardViews = new HashMap<>();

    private CardView() {
    }

    static CardView getInstance() {
        if (instance == null) {
            instance = new CardView();
        }
        return instance;

    }

    CardView.Card getCardViewCardToCardsCard(Cards.Card c) {
        if (!cardViews.containsKey(c.handle)) {
            cardViews.put(c.handle, getCard(c));
        }
        return cardViews.get(c.handle);
    }

    private CardView.Card getCard(Cards.Card card) {
        ImageIcon norm = null, sel = null;
        String fn = "";
        try {
            fn = "/ressources/" + card.handle + ".png";
            InputStream in = getClass().getResourceAsStream(fn);
            if (in != null) {
                BufferedImage img = ImageIO.read(in);
                norm = new ImageIcon(img);
                BufferedImage img_sel = new BufferedImage(img.getColorModel(), img.copyData(null), img.isAlphaPremultiplied(), null);
                Graphics2D gfx = img_sel.createGraphics();
                gfx.setColor(new Color(0, 255, 255, 66));
                gfx.fill(new Rectangle2D.Float(2, 2, img.getWidth() - 4, img.getHeight() - 4));
                gfx.dispose();
                sel = new ImageIcon(img_sel);
            } else {
                System.err.println("couldn't read image at " + fn);
            }
        } catch (IOException e) {
            System.err.println("couldn't read image " + fn + ": " + e);
        }
        return new CardView.Card(norm, sel);
    }

    static class Card {

        public final ImageIcon icon, iconSel;

        private Card(ImageIcon icon, ImageIcon iconSel) {
            this.icon = icon;
            this.iconSel = iconSel;
        }
    }
}
