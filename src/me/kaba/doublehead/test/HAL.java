/*
 * Copyright (C) 2020-2021 kaba
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.kaba.doublehead.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import me.kaba.doublehead.middleware.Cards;
import me.kaba.doublehead.middleware.Helper;
import me.kaba.doublehead.middleware.PlayerMgr;
import me.kaba.doublehead.middleware.Savvy;
import me.kaba.doublehead.middleware.Session;

/**
 *
 * @author kaba
 */
class HAL implements Savvy {
    private enum Task { connect, playCard, vote };
    private enum State { starting, connecting, idle /*waiting for a hand*/, monitoring, twirl /*waiting to play next card*/, playing /*a card*/, gambled, deciding };
    /**
     * @param args the command line arguments
     */
    public static void main( String args[] ) {
        assert args.length == 2;
        HAL hal = new HAL( args[0], args[1] );
        hal.go();
    }

    private HAL( String host, String port ) {
        props = new Properties();
        props.setProperty( PK_HOST, host );
        props.setProperty( PK_PORT, port );
        props.setProperty( PK_USERNAME, "aaa" );
    }

    private void go() {
        Helper.putUntilPut( workpile, Task.connect );
        do {
            try {
                Task t = workpile.take();
                    switch( t ) {
                        case connect:
                            connect();
                            break;
                        case playCard:
                            playCard();
                            break;
                        case vote:
                            vote();
                            break;
                        default:
                            System.out.println("INTERNAL: unimplemented TASK "+t );
                    }
                } catch( InterruptedException e ) {
                        System.out.println("INFO: "+e );
            }
        } while( gameC < 13 );
        if( timer != null ) {
            timer.cancel();
            timer = null;
        }
        if( ses != null ) {
            ses.end();
            ses = null;
        }
        System.out.println( "INFO: played 13 games - stopping" );
    }

    private void connect() {
        changeState("connect()", new State[]{State.starting}, State.connecting, true );
        System.out.println( "INFO: connecting for "+ props.getProperty(PK_USERNAME) );
        ses = new Session( this, props );
        ses.go();
    }

    private void playCard() {
        if( hand.isEmpty() )
            System.out.println( "ERROR: no cards left to play" );
        else {
            changeState("playCard()", new State[]{State.playing}, State.gambled, true );
            ses.playCard( hand.remove(0) );
        }
    }

    private void vote() {
        boolean ok=false;
        synchronized( state ) {
            trickC++;
            if( trickC > 12 )
                System.out.println( "ERROR: voting for trick no. " + trickC );
            ok = changeState("vote()", new State[]{State.deciding}, State.twirl, false );
        }
        if( ok )
            ses.voteTrick( ses.getGamePlayers().get(trickC%4) );
    }

    private boolean changeState( String where, State[] froms, State to, boolean anyway) {
        synchronized( state ) {
            kickStateTimer();
            Arrays.sort( froms );
            boolean transerror = Arrays.binarySearch(froms,state) < 0;
            if( transerror ) {
//                System.out.println("INTERNAL: in wrong state " + state + " to change to " + to + " in " + where + (anyway?" -- changing anyway":" -- staying in old state") );
                (new Exception("INTERNAL: in wrong state " + state + " to change to " + to + " in " + where + (anyway?" -- changing anyway":" -- staying in old state")))
                        .printStackTrace( System.out );
            }
            if( anyway || (!transerror) )
                state = to;
            return (! transerror);
        }
    }

//    private void checkPlayers( String method, List<String> p ) {
//        boolean wrong = ( p.size() == players.size() );
//        if( ! wrong )
//            for( int i=0; i<p.size(); i++ )
//                if( ! p.get(i).equals(players.get(i)) )
//                    wrong = true;
//        if( wrong )
//            System.out.println( "ERROR: got wrong players in " + method + ": " + p + " instead of expected " + players );
//    }

    @Override
    public void disconnect( String reason ) {
        changeState("disconnect(String)", new State[]{State.connecting}, State.starting, true );
        if( ses != null )
            ses.end();
        ses = null;
        System.out.println( "WARN: disconnected: " + reason +" --- retrying with new name..." );
        int low = Integer.parseInt( "aaa", 36 );
        int range = Integer.parseInt("zzz",36) - low;
        props.setProperty( PK_USERNAME, Integer.toString(rand.nextInt(range),36) );
        Helper.putUntilPut( workpile, Task.connect );
    }

    @Override
    public void envisTrickVoters( boolean visible, boolean enable ) {
        if( enable ) {
            changeState("envisTrickVoters(boolean,boolean)", new State[]{State.gambled}, State.deciding, true );
            Helper.putUntilPut( workpile, Task.vote );
        } else
            System.out.println( "INFO: envisTrickVoters(" + visible + ",false)" );
    }

    @Override
    public void enablePlayCard( boolean e ) {
        synchronized( state ) {
            if( e ) {
                changeState("enablePlayCard(boolean)", new State[]{State.twirl,State.deciding}, State.playing, true );
                Helper.putUntilPut( workpile, Task.playCard );
            } else
                System.out.println( "INFO: enablePlayCard(false)" );
        }
    }

    @Override
    public void setupGameView( List<String> orders ) {
        changeState("setupGameView()", new State[]{State.connecting}, State.idle, true );
        gameC = 0;
    }

    @Override
    public void showCurrent( List<PlayerMgr.Player> p, List<Cards.Card> cards ) {
    }

    @Override
    public void showMessageDialog( Object message, String title, int messageType ) {
        System.out.println( "ERROR type " + messageType + ": " + title + " --- " + message );
    }

    @Override
    public void removePlayed( Cards.Card card ) {
        // FIXME: change this in HAL as well ??
    }

    @Override
    public void showNewHand( List<Cards.Card> h ) {
        if( ! hand.isEmpty() )
            System.out.println( "ERROR: didn't play all my previous hand yet, " + hand.size() + " cards left" );
        hand = h;
        trickC = 0;
        if( hand.size() != 12 )
            System.out.println( "ERROR: received hand of " + hand.size() + " cards" );
        changeState("showNewHand(List<Cards.Card>)", new State[]{State.idle}, State.twirl, true );
    }

    @Override
    public void showPlayers( List<PlayerMgr.Player> players, int max ) {
        List<String> playerNames = new ArrayList<>( players.size() );
        for( PlayerMgr.Player p : players )
            playerNames.add( p.name );
        System.out.println( "INFO: showing " + players.size() + " players (max:" + max + ") in match: " + playerNames + "(playing: " + (ses==null?"no more":ses.getGamePlayers()) + ")" );
    }

    @Override
    public void showRecent( List<PlayerMgr.Player> p, List<Cards.Card> cards ) {
        System.out.println( "INFO: showing recent: " + p + " : " + cards );
    }

    @Override
    public void showTricks( List<PlayerMgr.Player> p, List<Integer> tricks ) {
        int s = 0;
        for( int t : tricks )
            s += t;
        if( (0>s) || (s>12) )
            System.out.println( "ERROR: showing tricks: " + p + " : " + tricks + " -- (" + s + ")" );
        else
            System.out.println( "INFO: showing tricks: " + p + " : " + tricks + " -- (" + s + ")" );
    }

    @Override
    public void jubilate( List<PlayerMgr.Player> players ) {
            System.out.println( "INFO: won trick" );
    }

    @Override
    public void startObserving() {
        changeState("startObserving()", new State[]{State.idle}, State.monitoring, true );
    }

    @Override
    public void stopObserving() {
        changeState("stopObserving()", new State[]{State.monitoring}, State.idle, true );
    }

    @Override
    public void showTricksDialog( List<List<Cards.Card>> tricks ){
        System.out.println( "INFO: won " + tricks.size() + " tricks" );
        gameC++;
    }

    private synchronized void kickStateTimer() {
        if( timer != null ) {
            timer.cancel();
            timer = null;
        }
        timer = new Timer();
        timer.schedule( new TimerTask() {
            @Override
            public void run() {
                System.out.println( "STATE: " + state + ", " + gameC + " games, " + trickC + " tricks, " + hand + ", " + players + ", " + workpile );
                if( (players!=null) && props.getProperty(PK_USERNAME).equals(players.get(0)) && (ses!=null) )
                    ses.ping();
            }
        }, 10000, 30000 );
    }

    private final Random rand = new Random( System.currentTimeMillis() );
    private State state = State.starting;
    private final Properties props;
    private final BlockingQueue<Task> workpile = new LinkedBlockingQueue<>( 100 );
    private Session ses;
    private List<String> players;
    private List<Cards.Card> hand = new ArrayList<>();
    private int gameC=0, trickC=0;
    private Timer timer;
}