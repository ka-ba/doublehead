/*
 * Copyright (C) 2020-2021 kaba
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.kaba.doublehead.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Inet4Address;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author kaba
 */
class TCPTest extends Thread {

    public static void main(String args[]) throws InterruptedException, IOException {
        new TCPTest().start();  // listending & printing server
        Thread.currentThread().sleep( 2000 );
        Socket sock = new Socket("127.0.0.1", 7777);
        PrintWriter out = new PrintWriter(sock.getOutputStream(), true);
        for( int i=0; i<10; i++) {
            out.println( "sending "+i );
            Thread.currentThread().sleep( 500 );
        }
        out.close();
        sock.close();
    }

    @Override
    public void run() {
        try {
            ServerSocket listen = new ServerSocket( 7777, 6, Inet4Address.getByName("127.0.0.1") );
            Socket accept = listen.accept();
            BufferedReader in = new BufferedReader(new InputStreamReader(accept.getInputStream()));
            while( true ) {
                String line = in.readLine();
                System.out.println( "received line: "+line );
            }
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }
}
