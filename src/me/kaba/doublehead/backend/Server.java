/*
 * Copyright (C) 2020-2021 kaba
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.kaba.doublehead.backend;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import me.kaba.doublehead.middleware.CMsg;
import me.kaba.doublehead.middleware.Coder;
import me.kaba.doublehead.middleware.Helper;
import me.kaba.doublehead.middleware.SMsg;
/**
 *
 * @author kaba
 */
class Server extends Thread {
    private static Server getInstance() {
        return ServerHolder.INSTANCE;
    }

    private static class ServerHolder {
        private static final Server INSTANCE = new Server();
    }

    public static void main(String args[]) throws IOException {
        if( args.length != 2  ) {
            System.err.println( "Need exactly 2 args: host port" );
            return;
        }
        System.out.println( "Server.main: starting on " + args[0] + " " + args[1] );
        int ses_count = 0;
        Server.getInstance().start();
        ServerSocket listen = new ServerSocket( Integer.parseInt(args[1]), 6, Inet4Address.getByName(args[0]) );
        while( true ) {
            try {
                Socket client = listen.accept();
                ServerSession s = new ServerSession( client, ses_count++ );
                synchronized( sessions ) {
                    sessions.put( s.id, s );
                    System.out.println( "Server.main: new " + s + ", now: " + sessions.size() + " - " + sessions.toString() );
                }
                s.go();
            } catch( IOException e ) {
                System.err.println( "Server.main: ignoring IOException while listening for clients: " + e );
                try { Thread.sleep( 2*1000 ); } catch (InterruptedException ex) { /* ignore */ }
            }
        }
    }

    @Override
    public void run() {
        while( true ) {
            try {
                SMsg msg = broadcastQ.take();
                synchronized( sessions ) {
                    if( msg.sessionId < 0 ) { // normal
                        broadcastStore.add( msg ); // sync'd to conform with feedMeHistory()
                        for( ServerSession s : sessions.values() )
                            s.acceptBroadcast( msg );    // must be outside broadcast sync!
                    } else { // special
                        ServerSession ses = sessions.get( msg.sessionId );
                        if( ses != null )
                            ses.gulpHistory( broadcastStore, currentMax() );
                        else
                            System.err.println( "Server.run: session "+msg.sessionId+", that requested history, gone" );
                    }
                }
            } catch( InterruptedException e ) {
                System.err.println( "Server.run: interrupted while taking broadcast - ignoring: "+e );
            } catch( Throwable t ) {
                System.err.println( "Server.run: exception while taking broadcast - trying to continue: "+t );
            }
        }
    }

    static void endSession( ServerSession s, String n ) {
        s.close();
        if( n != null )
            synchronized( attendees ) {
                boolean removed = attendees.remove( n );
                if( removed )
                    broadcast( Coder.vanished(n) );
                System.out.println( "Server.endSession: attende " + n + " removed, now: " + attendees.size() + " - " + attendees.toString() );
            }
        synchronized( sessions ) {
            sessions.remove( s.id );
        }
        System.out.println( "Server.endSession: " + s.toString() + " gone, now: " + sessions.size() + " - " + sessions.toString() );
    }

    static boolean addAttendee( String a ) {
        boolean added;
        synchronized( attendees ) {
            added = attendees.add( a );
            if( added )
                broadcast( Coder.appeared(a) );
        }
        if( added )
            System.out.println( "Server.addAttendee: attende " + a + " added, now: " + attendees.size() + " - " + attendees.toString() );
        return added;
    }

    private static void broadcast( SMsg msg ) { broadcast(msg,false); }

    private static void broadcast( SMsg msg, boolean special ) {
        synchronized( idGen ) {
            if( ! special )
                msg.setId(idGen.getAndIncrement());
            Helper.putUntilPut( broadcastQ, msg );
        }
    }

    static boolean broadcastCMsg(String sender, CMsg msg) {
        synchronized( idGen ) {
            if( msg.getId() != idGen.get() )
                return false;    // proposed id wouldn'd be the correct next one -> ignore, i.e. do not broadcast
            broadcast( new SMsg(sender,msg) );
            return true;
        }
    }

    static int currentMax() {
        return idGen.get() -1;
    }

    static void feedMeHistory( int ses_id ) {
        broadcast( new SMsg(ses_id), true );
    }

    private final static Map<Integer,ServerSession> sessions = new TreeMap<>();
    private final static Set<String> attendees  = new HashSet<>();
    private final static AtomicInteger idGen = new AtomicInteger();
    private final static BlockingQueue<SMsg> broadcastQ = new PriorityBlockingQueue<>();
    private final static Vector<SMsg> broadcastStore = new Vector<>(3000, 1000);
}