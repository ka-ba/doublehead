/*
 * Copyright (C) 2020-2021 kaba
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.kaba.doublehead.backend;

import me.kaba.doublehead.middleware.CMsg;
import me.kaba.doublehead.middleware.Coder;
import me.kaba.doublehead.middleware.Helper;
import me.kaba.doublehead.middleware.SMsg;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Collection;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

/**
 *
 * @author kaba
 */
class ServerSession implements Comparable<ServerSession> {
    ServerSession( Socket c, int myid ) {
        client = c;
        id = myid;
        playersName = null;
        sendQ = new PriorityBlockingQueue<>();
        accepting = false;
        receiver = new Receiver( this );
        sender = new Sender( this );
    }

    void go() {
        receiver.start();
        sender.start();
    }

    private synchronized void sendLocal( SMsg msg ) {
        assert sender != null;
        if( sender != null ) {
            msg.setMax( Server.currentMax() );
            Helper.putUntilPut( sendQ, msg );
        } else
            System.err.println( toString()+" ServerSession.sendLocal: without sender dropping: "+msg.toDebugString() );
    }

    private void loggedIn() {
        Server.feedMeHistory( id );
    }

    void gulpHistory( Collection<SMsg> hist, int max ) {
        synchronized( AcceptanceMon ) {
            assert sender != null;
            if( sender != null ) {
                for( SMsg b : hist )
                    Helper.putUntilPut( sendQ, b );
                accepting = true;
            }
        }
    }

    boolean acceptBroadcast( SMsg msg ) {
        synchronized( AcceptanceMon ) {
            if( accepting ) {
                Helper.putUntilPut( sendQ, msg );
                return true;
            }
            return false;
        }
    }

    private boolean handleSessionMsg( CMsg msg ) {
        switch (msg.cmd) {
            case Coder.CMD_REGISTRATION:
                if (playersName != null) { // not allowed to change name (meaning: not allowed to re-register)
                    sendLocal( Coder.rejectName(msg.args) );
                    return true;
                }
                boolean added = Server.addAttendee( msg.args );
                if (!added) {
                    sendLocal( Coder.rejectName(msg.args) );
                    return true;
                } else {
                    playersName = msg.args;
                    sendLocal( Coder.acceptName(msg.args) );
                    loggedIn();
                    return true;
                }
                // break;
        }
        return false;
    }

    private boolean broadcastCMsg( CMsg msg ) {
        return Server.broadcastCMsg( playersName, msg );
    }
    
    private synchronized void receiverStopping() {
        receiver = null;
        if( sender != null )
            sender.halt();
        maybeEndSession();
    }

    private synchronized void senderStopping() {
        sender = null;
        if( receiver != null )
            receiver.halt();
        maybeEndSession();
    }

    private synchronized void maybeEndSession() {
        if( (receiver==null) && (sender==null) )
            Server.endSession( this, playersName );
    }

    void close() {
        receiver = null;
        sender = null;
        playersName = null;
        try {
            client.close();
        } catch (IOException e) {
            System.err.println("ServerSession.close: ignoring IOException while closing client: " + e);
        }
    }

    @Override
    public String toString() {
        return "ServerSession:"+(client!=null?client.getPort():-1)+":"+(playersName!=null?playersName:"[]")+":"+(sender!=null?"S":"")+(receiver!=null?"R":"");
    }

    @Override
    public int compareTo( ServerSession o ) {
        return Integer.compare( id, o.id );
    }

    private class Receiver extends Thread {
        private Receiver( ServerSession s ) {
            session = s;
            in = null;
        }

        @Override
        public void run() {
            try {
                in = new BufferedReader(new InputStreamReader(client.getInputStream()));
                receiving = true;
                do {
                    try {
                        String msgstr = in.readLine();
                        if( msgstr == null )
                            throw new IOException( "null read" );
                        System.out.println( session.toString()+" <-: "+(msgstr.length()>12?msgstr.substring(0,12):msgstr)+"..." );
                        CMsg msg = Coder.parseClientMsg( msgstr );
                        if( ! session.handleSessionMsg(msg) )
                            session.broadcastCMsg(msg);
                    } catch( IllegalArgumentException e ) {
                        System.err.println( session.toString()+" Receiver.run: ignoring ill-formed message: "+e );
                    }
                } while( receiving );
            } catch( IOException e ) {
                System.err.println( session.toString()+" Receiver.run: IOException: "+e );
            } finally {
                try {
                    receiving = false;
                    if( in != null )
                        in.close();
                    session.receiverStopping();
                    System.out.println( "Server-Receiver exiting for "+session.toString() );
                } catch( IOException e ) {
                    System.err.println( session.toString()+" Receiver.run: ignoring IOException while closing: "+e );
                }
            }
        }

        private void halt() {
            receiving = false;
            interrupt();
        }

        private ServerSession session=null;
        private BufferedReader in;
        private boolean receiving;
    }

    private class Sender extends Thread {

        private Sender( ServerSession s ) {
            session = s;
            out = null;
            sending = false;
        }

        @Override
        public void run() {
            try {
                out = new PrintWriter( client.getOutputStream(), true );
                sending = true;
                do {
                    try {
                        SMsg msg = session.sendQ.take();
                        msg.setMax( Server.currentMax() );
                        out.println( msg.toString() );  // FIXME: doesn't seem to throw - keep eye on that!
                        System.out.println( session.toString()+" ->: "+msg.toDebugString() );
                    } catch( InterruptedException e ) {
                        System.out.println( session.toString()+" interrupted while taking msg - "+(sending?"retrying":"exiting") );
                    }
                } while( sending );
            } catch( IOException e ) {
                System.err.println( session.toString()+" Sender.run: IOException: "+e );
            } finally {
                sending = false;
                if( out != null )
                    out.close();
                session.senderStopping();
                System.out.println( "Server-Sender exiting for "+session.toString() );
            }
        }

        private void halt() {
            sending = false;
            interrupt();
        }

        private ServerSession session=null;
        private PrintWriter out;
        private boolean sending;
    }

    private final Socket client;
    final int id;
    private Receiver receiver;
    private Sender sender;
    private String playersName;
    private BlockingQueue<SMsg> sendQ;
    private boolean accepting;
    private final Object AcceptanceMon = new Object();
}
