/*
 * Copyright (C) 2020-2021 kaba
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.kaba.doublehead.middleware;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

import java.io.Serializable;

/**
 *
 * @author kaba
 */
public class Cards {

    public class Card implements Serializable {

        private Card( String h, String tt, boolean t, int p ) {
            handle = h;
            toolTip = tt;
            trump = t;
            points = p;
        }

        @Override
        public String toString() { return "Card{" + handle + "}"; }
        public final String handle, toolTip;
        public final boolean trump;
        public final int points;
    }

    List<Collection<Card>> deal() {
        List<Collection<Card>> result;
        int faulty_hands;
        do {
            List<Card> deck = new LinkedList<>();
            deck.addAll(map.values());
            deck.addAll(map.values());
            result = new ArrayList<>(4);
            for (int i = 0; i < 4; i++) {
                Collection<Card> hand = new ArrayList<>(12);
                for (int j = 0; j < 12; j++) {
                    hand.add(deck.remove(random.nextInt(deck.size())));
                }
                assert hand.size() == 12;
                result.add(hand);
            }
            assert result.size() == 4;
            assert deck.size() == 0;
            faulty_hands = 0;
            for (Collection<Card> hand : result) {
                if (!(testHand9(hand) && testHandT(hand))) {
                    faulty_hands++;
                }
            }
        } while (faulty_hands > 0);
        return result;
    }

    static boolean testHand9 (Collection<Card> hand) {
        int n = 0;
        for( Card aaa : hand){
            if( aaa.handle.contains("9") )
                n = n+1;
        }
        if (n>=5)
            return false;
        else return true;
    }
        
    static boolean testHandT (Collection<Card> hand) {
        int n = 0;
        for( Card aaa : hand){
            if( aaa.trump )
                n = n+1;
        }
        if (n<=3)
            return false;
        else return true;
    }

    List<String> getOrderNames() {
        return new ArrayList<>( sorts.keySet() );
    }

    public void sort( List<Card> cards, String ordername ) {
        Map<String,Integer> order = sorts.get( ordername );
        if( order != null )
            cards.sort( (c1,c2) -> order.get(c1.handle).compareTo(order.get(c2.handle)) );
        else
            System.err.println( "WARNING: cannot sort by "+ordername+" - onknown sort order" );
    }

    static List<Cards.Card> resolveHandles( List<String> handles ) {
        List<Card> hand = new ArrayList<>( handles.size() );
        for( String h : handles ) {
            Card c = getInstance().map.get( h );
            if( c != null )
                hand.add( c );
            else
                System.err.println( "Cards: cannot resolve handle "+h );
        }
        return hand;
    }

    public static Cards getInstance() {
        return CardsHolder.INSTANCE;
    }
    
    private static class CardsHolder {
        private static final Cards INSTANCE = new Cards();
    }

    private Cards() {
        Map<String, Card> m = new HashMap<>();
        m.put( "KA", new Card("KA", "As Kreuz", false, 11) );
        m.put( "KK", new Card("KK", "König Kreuz", false, 4) );
        m.put( "KD", new Card("KD", "Dame Kreuz", true, 3) );
        m.put( "KB", new Card("KB", "Bube Kreuz", true, 2) );
        m.put( "KZ", new Card("KZ", "10 Kreuz", false, 10) );
        m.put( "K9", new Card("K9", "9 Kreuz", false, 0) );
        m.put( "PA", new Card("PA", "As Pik", false, 11) );
        m.put( "PK", new Card("PK", "König Pik", false, 4) );
        m.put( "PD", new Card("PD", "Dame Pik", true, 3) );
        m.put( "PB", new Card("PB", "Bube Pik", true, 2) );
        m.put( "PZ", new Card("PZ", "10 Pik", false, 10) );
        m.put( "P9", new Card("P9", "9 Pik", false, 0) );
        m.put( "HA", new Card("HA", "As Herz", false, 11) );
        m.put( "HK", new Card("HK", "König Herz", false, 4) );
        m.put( "HD", new Card("HD", "Dame Herz", true, 3) );
        m.put( "HB", new Card("HB", "Bube Herz", true, 2) );
        m.put( "HZ", new Card("HZ", "10 Herz", true, 10) );
        m.put( "H9", new Card("H9", "9 Herz", false, 0) );
        m.put( "kA", new Card("kA", "As Karo", true, 11) );
        m.put( "kK", new Card("kK", "König Karo", true, 4) );
        m.put( "kD", new Card("kD", "Dame Karo", true, 3) );
        m.put( "kB", new Card("kB", "Bube Karo", true, 2) );
        m.put( "kZ", new Card("kZ", "10 Karo", true, 10) );
        m.put( "k9", new Card("k9", "9 Karo", true, 0) );
        map = m;
        assert map.size() == 24;
        back = new Card( "back", "Rück", false, 0 );
        random = new Random();
        populateSortOrder( "simpel", new String[]{ "HZ", "KD", "PD", "HD", "kD", "KB", "PB", "HB", "kB", "kA", "kZ", "kK", "k9",
            "KA", "KZ", "KK", "K9", "PA", "PZ", "PK", "P9", "HA", "HK", "H9" } );
        populateSortOrder( "s-Damen", new String[]{ "KD", "PD", "HD", "kD",
            "KA", "KZ", "KK", "KB", "K9", "PA", "PZ", "PK", "PB", "P9", "HA", "HZ", "HK", "HB", "H9", "kA", "kZ", "kK", "kB", "k9" } );
        populateSortOrder( "s-Buben", new String[]{ "KB", "PB", "HB", "kB",
            "KA", "KZ", "KK", "KD", "K9", "PA", "PZ", "PK", "PD", "P9", "HA", "HZ", "HK", "HD", "H9", "kA", "kZ", "kK", "kD", "k9" } );
        populateSortOrder( "s-Ivan", new String[]{ "KK", "PK", "HK", "kK", "KD", "PD", "HD", "kD", "KB", "PB", "HB", "kB",
            "KA", "KZ", "K9", "PA", "PZ", "P9", "HA", "HZ", "H9", "kA", "kZ", "k9" } );
        populateSortOrder( "s-veggi", new String[]{ "KA", "KZ", "KK", "KD", "KB", "K9", "PA", "PZ", "PK", "PD", "PB", "P9",
            "HA", "HZ", "HK", "HD", "HB", "H9", "kA", "kZ", "kK", "kD", "kB", "k9" } );
    }

    private void populateSortOrder( String name, String[] order ) {
        assert order.length == 24;
        Map<String,Integer> map = new HashMap<>();
        for( int i=0; i<order.length; i++ )
            map.put( order[i], i);
        assert map.size() == 24;
        sorts.put( name, map );
    }

    final Map<String,Card> map;
    public final Card back;
    private final Random random;
    private Map<String,Map<String,Integer>> sorts = new TreeMap<>();
}
