/*
 * Copyright (C) 2020-2021 kaba
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.kaba.doublehead.middleware;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kaba
 */
class CMsgArgs {
    
    CMsgArgs(CMsg msg) {
        id_ = msg.id;
        cmd_ = msg.cmd;
        switch (msg.cmd) {
            case Coder.CMD_NEW_HAND:
                String[] split = msg.args.split(" ");
                assert split.length == 13;
                trgtPlayer_ = split[0];
                hand_ = new ArrayList<>(12);
                for (int i = 1; i < split.length; i++) {
                    hand_.add(split[i]);
                }
                break;
            case Coder.CMD_PLAY_CARD:
                /*String[]*/
                split = msg.args.split(" ");
                assert split.length == 2;
                srcPlayer_ = split[0];
                cardHandle_ = split[1];
                break;
            case Coder.CMD_VOTE_TRICK:
                /*String[]*/
                split = msg.args.split(" ");
                assert split.length == 3;
                srcPlayer_ = split[0];
                trgtPlayer_ = split[1];
                voteBase_ = Integer.parseInt( split[2], 36 );
                break;
        }
    } /*String[]*/ /*String[]*/

    public int id() { return id_; }

    public String srcPlayer() {
        switch (cmd_) {
            case Coder.CMD_PLAY_CARD:
            case Coder.CMD_VOTE_TRICK:
                return srcPlayer_;
            default:
                throw new IllegalArgumentException("there's no src player in cmd " + cmd_);
        }
    }

    public String trgtPlayer() {
        switch (cmd_) {
            case Coder.CMD_NEW_HAND:
            case Coder.CMD_VOTE_TRICK:
                return trgtPlayer_;
            default:
                throw new IllegalArgumentException("there's no trgt player in cmd " + cmd_);
        }
    }

    public List<String> hand() {
        switch (cmd_) {
            case Coder.CMD_NEW_HAND:
                return hand_;
            default:
                throw new IllegalArgumentException("there's no Hand in cmd " + cmd_);
        }
    }

    public String cardHandle() {
        switch (cmd_) {
            case Coder.CMD_PLAY_CARD:
                return cardHandle_;
            default:
                throw new IllegalArgumentException("there's no card handle in cmd " + cmd_);
        }
    }

    public int voteBase() {
        switch (cmd_) {
            case Coder.CMD_VOTE_TRICK:
                return voteBase_;
            default:
                throw new IllegalArgumentException("there's no vote base in cmd " + cmd_);
        }
    }
    private int id_;
    private String cmd_ = null;
    private String srcPlayer_ = null;
    private String trgtPlayer_ = null;
    private List<String> hand_ = null;
    private String cardHandle_ = null;
    private int voteBase_ = 0;
}
