/*
 * Copyright (C) 2020-2021 kaba
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.kaba.doublehead.middleware;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;
import javax.swing.JOptionPane;

/** holds all session data and controls all session actions, may reconnect without session state lost
 *
 * @author kaba
 */
public class Session {

    public Session( Savvy m_fra, Properties p ) {
        mFrame = m_fra;
        props = p;
        state = new SesState();
        amChefe = false;
        pmgr = new PlayerMgr( mFrame, this, props.getProperty(Savvy.PK_USERNAME) );
        com = null;
    }
  
    @Override
    protected void finalize() throws Throwable {
        try {
            end();
        } finally {
            super.finalize();
        }
    }

    public void go() {
        com = new Communicator( this, props );
        com.start();
        com.send( Coder.register(props.getProperty(Savvy.PK_USERNAME)) );
    }

    public void end() {
        System.out.println("Session.end: "+(com==null?"not":"")+" ending communicator");
        if( com != null )
            com.end();
        com = null;
    }

    void receive( SMsg msg ) {
        if( handleServerMsg(msg) )
            return;
        handleBroadcast( msg );
    }

    private boolean handleServerMsg( SMsg msg ) {
        if( Coder.CMD_NAME_ACCEPT.equals(msg.cmd) || Coder.CMD_NAME_REJECT.equals(msg.cmd) )
            System.out.println( "Session: handle as server msg: "+msg.toDebugString()+(msg.amCatchingUp()?" - catching up":"") );
        switch( msg.cmd ) {
            case Coder.CMD_NAME_ACCEPT:
                mFrame.setupGameView( Cards.getInstance().getOrderNames() );
                return true;
                // break;
            case Coder.CMD_NAME_REJECT:
                nameRejected();
                return true;
                // break;
        }
        return false;
    }
    
    private void handleBroadcast( SMsg msg ) {
        System.out.println( "Session: handle as broadcast: "+msg.toDebugString()+(msg.amCatchingUp()?" - catching up":"") );
        try {
            synchronized( state ) {
                doActions( state.happend(new StateEvent(msg,amChefe,fObserving,null)) );
            }
        } catch( Exception e ) {
            e.printStackTrace();
            mFrame.showMessageDialog( e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE );
        }
    }
    
    private void nameRejected() {
        end();
        mFrame.disconnect( "players name "+props.getProperty(Savvy.PK_USERNAME)+" rejected" );
    }

    private void handReceived( CMsgArgs args ) {
        if( ! args.trgtPlayer().equals(props.getProperty(Savvy.PK_USERNAME)) ) { // this for me?
            foreignHandCount++;
        } else {
            assert ownHandCount == 0;
            ownHandCount++;
            List<Cards.Card> hand = Cards.resolveHandles( args.hand() );
            assert hand.size() == 12;
            mFrame.showNewHand( hand );
        }
    }

    private void playedCardReceived( CMsgArgs args ) {
        int p_idx = gamersNames.indexOf( args.srcPlayer() );
        if( p_idx < 0 )
            throw new IllegalArgumentException( "fatal: player "+args.srcPlayer()+" played card "+args.cardHandle()+", but is no active player ("+gamersNames+")" );
        if( ! Cards.getInstance().back.equals(trick.get(p_idx)) )
            throw new IllegalArgumentException( "fatal: player "+args.srcPlayer()+" just played card "+args.cardHandle()+", but played "+trick.get(p_idx).handle+" before during this same trick" );
        Cards.Card c = Cards.getInstance().map.get( args.cardHandle() );
        if( c == null )
            throw new IllegalArgumentException( "fatal: player "+args.srcPlayer()+" just played unknown card "+args.cardHandle() );
        if( args.srcPlayer().equals(props.getProperty(Savvy.PK_USERNAME)) ) // 't was me
            mFrame.removePlayed( c );
        trick.set( p_idx, c );
        mFrame.showCurrent( gamePlayers, trick );
    }

    private boolean trickComplete() {
        assert trick.size() == 4;
        for( Cards.Card cc : trick )
            if( cc.equals( Cards.getInstance().back) )
                return false;
        return true;
    }

    private void trickVoteReceived( CMsgArgs args ) {
        if( (voteBase>=0) && (voteBase<=args.voteBase()) ) {
            int v_idx = gamersNames.indexOf( args.srcPlayer() );
            if( v_idx < 0 )
                throw new IllegalArgumentException( "fatal: player "+args.srcPlayer()+" voted for "+args.trgtPlayer()+", but the former is no active player ("+gamersNames+")" );
            int r_idx = gamersNames.indexOf( args.trgtPlayer() );
            if( r_idx < 0 )
                throw new IllegalArgumentException( "fatal: player "+args.srcPlayer()+" voted for "+args.trgtPlayer()+", but the latter is no active player ("+gamersNames+")" );
            receiver.set( v_idx, r_idx );
        } else
            System.err.println( "INFO: trickVoteReceived: ignoring late or illegal vote for base "+CMsg.id2str(args.voteBase())+", while expecting "+CMsg.id2str(voteBase) );
    }

    private int trickWinner() {
        // check for majority
        int votes[] = new int[]{0,0,0,0};
        for( int r : receiver )
            if( r > -1 )
                votes[r]++;
        for( int i=0; i<gamePlayers.size(); i++ )
            if( votes[i] > 2 ) {  // i is winner of trick
                return i;
            }
        return -1; // not decided yet
    }

    private void assignTrick() {
        int w = trickWinner();
        assert w >= 0;
        assert w < gamePlayers.size();
        voteBase = -1 * Math.abs(voteBase);
        tricks.get(w).add( trick );
        mFrame.showRecent( gamePlayers, trick );
        showTrickCount();
        if( gamePlayers.get(w).me )
            mFrame.jubilate(gamePlayers);
        initVote(); // avoid illegal second counting
    }

    private void showTrickCount() {
        List<Integer> ts = new ArrayList<>( gamePlayers.size() );
        for( int i=0; i<gamePlayers.size(); i++ )
            ts.add( tricks.get(i).size() );
        mFrame.showTricks( gamePlayers, ts );
    }

    private void initGame() {
        pmgr.nextGame();
        gamePlayers = pmgr.getActive();
        gamersNames = new ArrayList<>();
        for(PlayerMgr.Player p:gamePlayers)
            gamersNames.add (p.name);
        // also possible :  gamePlayers.forEach( p -> {gamersNames.add(p.name);} );
        tricks = new ArrayList<>( gamePlayers.size() );
        gamePlayers.forEach( p -> {
            tricks.add( new ArrayList<>(12) );
        });
        initTrick();
        mFrame.showRecent( gamePlayers, trick );  // shows backsides here as well
        showTrickCount();
    }

    private void initTrick() {
        trick = new ArrayList<>( gamePlayers.size() );
        gamePlayers.forEach( p -> {
            trick.add( Cards.getInstance().back );
        });
        mFrame.envisTrickVoters( false, false );
        mFrame.showCurrent( gamePlayers, trick );
        // recent shown in trickVoted()
    }

    public void playCard( Cards.Card c ) {
        assert( ! fObserving );
        synchronized( state ) {
            doActions( state.happend(new StateEvent(StateEvent.Clicks.sendcard,
                    Coder.cmdPlayCard(props.getProperty(Savvy.PK_USERNAME),c.handle),amChefe,fObserving,null)) );
        }
    }

    public void voteTrick( String p_name ) {
        assert( ! fObserving );
        try {
            synchronized( state ) {
                doActions( state.happend(new StateEvent(StateEvent.Clicks.sendvote,
                        Coder.cmdVoteTrick(props.getProperty(Savvy.PK_USERNAME),p_name,voteBase),amChefe,fObserving,null)) );
        }
        } catch( IllegalStateException e) {
            System.err.println( "Session.voteTrick: too late to send my vote: "+e.getMessage() );
        }
    }

    public void ping() {
        com.send( Coder.cmdPing() );
    }

    void beChefe() {
        amChefe = true;
    }

    public List<String> getGamePlayers() { // delivers a copy - for HAL use -- FIXME: change no immutable list ????
        if( (gamePlayers==null) || (gamePlayers.isEmpty()) )
            return new ArrayList<>(0);
        List<String> gp = new ArrayList<>( gamersNames );
        return gp;
    }

    private void doActions( SesState.Action act ) {
        for( SesState.Actions a : act.actions ) {
            switch( a ) {
                case addplayer:
                    pmgr.appeared( act.event.sMsg.args );
                    break;
                case remplayer:
                    pmgr.vanished( act.event.sMsg.args );
                    break;
                case testplayers:
                    doActions( state.happend(new StateEvent(pmgr.count()>=4,amChefe,fObserving,act.prev,act.event)) ); // FIXME: count only online players
                    break;
                case sendngonce:
                    if( ! fNGsent ) {
                        assert amChefe;
                        com.send( Coder.cmdNextGame() );
                    }
                    fNGsent = true;
                    break;
                case initgame:
                    initGame();
                    break;
                case counthands:
                    handReceived( new CMsgArgs(act.event.sMsg) );
                    break;
                case testhands:
                    doActions( state.happend(new StateEvent((foreignHandCount+ownHandCount)>=4,amChefe,fObserving,act.prev,act.event)) );
                    break;
                case testmyhand:
                    assert ownHandCount <= 1;
                    assert gamersNames.contains(props.getProperty(Savvy.PK_USERNAME)) == (ownHandCount>=1);
                    doActions( state.happend(new StateEvent(ownHandCount>=1,amChefe,fObserving,act.prev,act.event)) );
                    break;
                case guiplaycardonce:
                    if( ! fGUIplaycard ) 
                        mFrame.enablePlayCard( true );
                    fGUIplaycard = true;
                    break;
                case countcards:
                    playedCardReceived( new CMsgArgs(act.event.sMsg) );
                    break;
                case testcards:
                    doActions( state.happend(new StateEvent(trickComplete(),amChefe,fObserving,act.prev,act.event)) );
                    break;
                case initvoting:
                    voteBase = act.event.sMsg.id;
                    initVote();
                    if( fObserving )
                        mFrame.removePlayed( Cards.getInstance().back );
                    break;
                case guivoting:
                    mFrame.envisTrickVoters( true, true );
                    break;
                case countvotes:
                    trickVoteReceived( new CMsgArgs(act.event.sMsg) );
                    break;
                case testvotes:
                    doActions( state.happend(new StateEvent(trickWinner()>=0,amChefe,fObserving,act.prev,act.event)) );
                    break;
                case assigntrick:
                    assignTrick();
                    break;
                case testtricks:
                    doActions( state.happend(new StateEvent(sumTricks()>=12,amChefe,fObserving,act.prev,act.event)) );
                    break;
                case cleartrick:
                    initTrick();
                    break;
                case clearflagsplus:
                    if(fObserving)
                            mFrame.stopObserving();
                    fObserving = fNGsent = fDealt = fGUIplaycard = false;
                    foreignHandCount = ownHandCount = 0;
                    break;
                case clearguipc:
                    fGUIplaycard = false;
                    break;
                case showtricks:
                    showTricks();
                    break;
                case dealonce:
                    if( ! fDealt ) {
                        assert amChefe;
                        deal();
                        fDealt = true;
                    }
                    break;
                case sendcard:
                    assert act.event.cMsg != null;
                    assert act.event.cMsg.cmd.equals( Coder.CMD_PLAY_CARD );
                    com.send( act.event.cMsg );
                    break;
                case sendvote:
                    assert act.event.cMsg != null;
                    assert act.event.cMsg.cmd.equals( Coder.CMD_VOTE_TRICK );
                    com.send( act.event.cMsg );
                    break;
                case keepcalm:
                    System.out.println( "keeping my calm" );
                    break;
                case setobserving:
                    fObserving = true;
                    mFrame.startObserving();
                    break;
                case error:
                    throw new IllegalStateException( act.errStr );
                default:
                    throw new IllegalArgumentException( "don't know how to do "+a );
            }
        }
    }

    private void deal() {
        List<PlayerMgr.Player> players = pmgr.getActive();
        List<Collection<Cards.Card>> hands = Cards.getInstance().deal();
        assert players.size() == hands.size();
        for( int i=0; i<hands.size(); i++ )
            com.send( Coder.cmdNewHand(players.get(i).name,hands.get(i).stream().map(c -> c.handle).collect(Collectors.toList())) );
    }

    private void showTricks() {
        int my_idx = gamersNames.indexOf( props.getProperty(Savvy.PK_USERNAME) );
        if( my_idx < 0 )
            throw new IllegalStateException( "cannot show my tricks when I didn't play" );
        mFrame.showTricksDialog( tricks.get(my_idx) );
    }

    private void initVote() {
        receiver = new ArrayList<>( gamePlayers.size() );
        gamePlayers.forEach( p -> {
            receiver.add( -1 );
        });
    }

    private int sumTricks() {
        int c = 0;
        c = tricks.stream().map( pt -> pt.size() ).reduce( c, Integer::sum );
        assert (c>=0) && (c<13);
        return c;
    }

    private final Savvy mFrame;
    private final Properties props;
    private final SesState state;
    private Communicator com;
    private final PlayerMgr pmgr;
    private boolean amChefe=false, fObserving=false, fNGsent=false, fDealt=false, fGUIplaycard=false;
    private List<PlayerMgr.Player> gamePlayers;
    private List<String> gamersNames;
    private int foreignHandCount = 0, ownHandCount = 0, voteBase = -1;
    private List<Cards.Card> trick;
    private List<List<List<Cards.Card>>> tricks;
    private List<Integer> receiver;
}
