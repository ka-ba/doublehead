/*
 * Copyright (C) 2020-2021 kaba
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.kaba.doublehead.middleware;

import java.util.List;

/**
 *
 * @author kaba
 */
public interface Savvy {
    public final static String PK_HOST="host";
    public final static String PK_PORT="port";
    public final static String PK_USERNAME="username";
    void disconnect( String reason );
    void envisTrickVoters( boolean visible, boolean enable );
    void enablePlayCard( boolean e );
    void setupGameView( List<String> orders );
    void showCurrent( List<PlayerMgr.Player> players, List<Cards.Card> cards );
    void showMessageDialog( Object message, String title, int messageType );
    void showNewHand( List<Cards.Card> hand );
    void removePlayed( Cards.Card card );
    void showPlayers( List<PlayerMgr.Player> players, int max );
    void showRecent( List<PlayerMgr.Player> players, List<Cards.Card> cards );
    void showTricks( List<PlayerMgr.Player> players, List<Integer> tricks );
    void jubilate( List<PlayerMgr.Player> players );
    void startObserving();
    void stopObserving();
    void showTricksDialog( List<List<Cards.Card>> tricks );
}
