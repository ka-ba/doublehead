/*
 * Copyright (C) 2020-2021 kaba
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.kaba.doublehead.middleware;

/**
 *
 * @author kaba
 */
public class SMsg extends CMsg {
    
    SMsg( String s, int i, int m, String c, String a ) {
        super(i, c, a);
        max = m;
        sender = s;
        sessionId = -1;
    }

    SMsg( String s, String c, String a ) {
        this( s, -1, -1, c, a );
    }

    public SMsg( String s, CMsg msg ) {
        super( msg );
        max = -1;
        sender = s;
        sessionId = -1;
    }

    /** CAUTION: special const for inside server use only */
    public SMsg( int ses_id ) {
        super( -1, "special", "special" );
        sessionId = ses_id;
        sender = "special";
        max = -1;
    }

    public SMsg setId( int i ) { // Server needs to do this
        id = i;
        return this;
    }

    public SMsg setMax( int i ) { // ServerSession needs to do this
        max = i;
        return this;
    }

    public String maxStr() {
        return id2str( max );
    }

    public boolean amCatchingUp() {
        return id != max;
    }

    @Override
    public String toDebugString() {
        return toDebugStringInternal( " "+maxStr()+" "+sender );
    }

    @Override
    public String toString() {
        return idStr() + " " + maxStr() + " " + sender + " " + cmd + " " + args;
    }

    private int max;
    public final String sender;
    public final int sessionId; // not transmitted, used inside server only
}