/*
 * Copyright (C) 2020-2021 kaba
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.kaba.doublehead.middleware;

/**
 *
 * @author kaba
 */
public class CMsg implements Comparable<CMsg>{
    CMsg(int i, String c, String a) {
        id = i;
        cmd = c;
        args = a;
    }

    CMsg(int i, String c, String a, boolean o) {
        this( i, c, a );
        oneShot = o;
    }

    CMsg( CMsg msg ) {
        this( msg.id, msg.cmd, msg.args, msg.oneShot );
    }

    public int getId() {
        return id;
    }

    public boolean isOneShot() {
        return oneShot;
    }

    public String idStr() {
        return id2str( id );
    }

    public static String id2str( int i ) {
        return Integer.toString( i, 36 );
    }

    public String toDebugString() {
        return toDebugStringInternal( "" );
    }

    protected String toDebugStringInternal( String sender) {
        try {
        switch( cmd ) {
            case Coder.CMD_NEW_HAND:
            case Coder.CMD_PLAY_CARD:
                return idStr() + sender + " " + Coder.CMD_NAMES.get(cmd) + " " + args.substring(0,args.indexOf(" "))+ " ...";
            default:
                return idStr() + sender + " " + Coder.CMD_NAMES.get(cmd) + " " + args;
        }
        } catch( Exception e ) {
            return "CANNOT debStr '" + idStr() + sender + " " + cmd + " " + args +"' - Exception: " + e;
        }
    }

    @Override
    public String toString() {
        return idStr() + " " + cmd + " " + args;
    }

    public boolean contentsMatch( CMsg other ) {
        return cmd.equals(other.cmd) && args.equals(other.args);
    }

    @Override
    public int compareTo( CMsg o ) {
        if( id == o.id )
            return cmd.compareTo( o.cmd );
        return Integer.compare(id, o.id);
    }

    protected int id;
    public final String cmd;
    public final String args;
    private boolean oneShot = false;  // meaningles in derived SMsg
}