/*
 * Copyright (C) 2020-2021 kaba
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.kaba.doublehead.middleware;

import java.util.concurrent.BlockingQueue;

/**
 *
 * @author kaba
 */
public class Helper {
    public static <T> void putUntilPut( BlockingQueue<T> q, T t ) {
        boolean done = false;
        do {
            try {
                q.put( t );
                done = true;
            } catch( InterruptedException e ) {
                System.out.println("INFO: interrupted while trying to put-u-put "+t+", trying again - "+e );
            }
        } while( ! done );
    }
}