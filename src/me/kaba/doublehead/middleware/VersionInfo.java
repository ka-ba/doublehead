/*
 * Copyright (C) 2021 kaba
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.kaba.doublehead.middleware;

/**
 *
 * @author kaba
 */
public class VersionInfo {
    /** versionString should match versionNumber or reflect some transient status */
    public final String versionString = "01.06.00";
    /** versionNumber should match latest tag */
    public final int versionNumber = 010600;
    /** builds with the same compatVersion should play well together */
    public final int compatVersion = 8;

    @Override
    public String toString() {
        return versionString + " (" + compatVersion + ")";
    }

    private VersionInfo() {
    }
    
    public static VersionInfo getInstance() {
        return VersionInfoHolder.INSTANCE;
    }
    
    private static class VersionInfoHolder {

        private static final VersionInfo INSTANCE = new VersionInfo();
    }
}
