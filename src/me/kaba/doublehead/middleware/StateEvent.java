/*
 * Copyright (C) 2020-2021 kaba
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.kaba.doublehead.middleware;

/**
 *
 * @author kaba
 */
class StateEvent {
    enum Events { received, clicked, tested };
    enum Clicks { sendcard, sendvote, ping };
    StateEvent( SMsg in, boolean c, boolean o, SesState p ) {
        event=Events.received; sMsg=in; cMsg=null; answer=false;click=Clicks.ping; chefe=c; observer=o; prev=p;
    }
    StateEvent( Clicks cl, CMsg out, boolean c, boolean o, SesState p ) {
        event=Events.clicked; sMsg=null; cMsg=out; answer=false; click=cl; chefe=c; observer=o; prev=p;
    }
    StateEvent( boolean a, boolean c, boolean o, SesState ps, StateEvent pe ) {
        event=Events.tested; sMsg=pe.sMsg; cMsg=pe.cMsg; answer=a; click=pe.click; chefe=c; observer=o; prev=ps;
    }
    final Events event;
    final SMsg sMsg;
    final CMsg cMsg;
    final boolean answer, chefe, observer;
    final Clicks click;
    final SesState prev;
}
