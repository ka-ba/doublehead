/*
 * Copyright (C) 2020-2021 kaba
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.kaba.doublehead.middleware;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 *
 * @author kaba
 */
class Communicator extends Thread {

    Communicator(Session s, Properties p) {
        super("DoubleHead-Communicator-Sender");
        sess = s;
        props = p;
        sock = null;
        out = null;
        in = null;
        lastId = -1;
        toSend = new LinkedBlockingQueue<>(10);
        echo = new LinkedBlockingQueue<>();
        currMsg = null;
        connected = false;
        running = true;
    }

    void end() {
        running = false;
        System.out.println("Communicator.end: ending");
        if( lis != null )
            lis.interrupt();
        interrupt();
    }

    void send( CMsg msg ) {
        System.out.println("Communicator.send: goin 2 enqueue "+msg.toDebugString());
        Helper.putUntilPut( toSend, msg );
    }

    @Override
    public void run() {
        lis = new Listener(this);
        lis.start();
        do {
            if( ! running )
                break;
            connect();
            while( connected && running ) {
                try {
                    if( sock.isOutputShutdown() )
                        throw new SocketException( "socket output shut down" );
                    if( currMsg == null )
                        currMsg = toSend.take();
                    assert currMsg != null;
                    currMsg.id = lastId + 1;
                    System.out.println("Communicator.run: really sending " + currMsg.toDebugString());
                    out.println( currMsg.toString() );
                    if( currMsg.isOneShot() )
                        currMsg = null;
                    else {
                        // listen for echo
                        SMsg received=null;
                        do {
                            try {
                                received = echo.take();
                                if( received.id == currMsg.id ) {
                                    if( received.contentsMatch(currMsg) )
                                        currMsg = null;
                                    else {
                                        if( received.sender.equals(props.getProperty(Savvy.PK_USERNAME)) )
                                            System.err.println("error: received echo of msg that I didn't send with this id; sent: "+currMsg.toDebugString()+"; echo: "+received.toDebugString()+" - resending");
                                        else
                                            System.err.println("my msg dropped due to id collision; sent: "+currMsg.toDebugString()+"; echo: "+received.toDebugString()+" - resending");
                                    }
                                }
                            } catch( InterruptedException e ) {
                                System.err.println("Communicator.run: interrupted on taking echo, retrying (endlessly), unless disconnected - "+e );
                            }
                        } while( (received!=null) && (currMsg!=null) && (received.id<currMsg.id) );
                    } // ! oneShot
                } catch( InterruptedException e ) {
                    System.err.println("Communicator.run: interrupted on taking msg2send, retrying (endlessly) - "+e );
                } catch( SocketException e ) {
                    connectionLost();
                }
            } // connected && running
        } while( running );
        running = false;  // just to be sure ...
        try {
            out.close();
            in.close();
        } catch( IOException e ) {
            System.err.println("Communicator.run: ignoring on out/in close: "+e);
        }
        try {
            sock.close();
        } catch( IOException e ) {
            System.err.println("Communicator.run: ignoring on sock close: "+e);
        }
        try {
            if( lis != null )
                synchronized( lis ) {
                    lis.notifyAll();
                }
        } catch( Throwable t) { /*ignore*/ }
        System.out.println( "Communicator.run: exiting " );
    }

    private void connect() {
        if (sock != null) {
            try { out.close(); in.close(); } catch( IOException e ) { /*ignore*/ }
            try {
                sock.close();
            } catch (IOException e) {
                System.err.println("Communicator.connect: ignoring IOException while closing: " + e);
            }
            in = null;
            out = null;
            sock = null;
        }
        try {
            System.err.println("Communicator.connect: goin2connect "+props.getProperty(Savvy.PK_HOST)+" : "+props.getProperty(Savvy.PK_PORT));
            sock = new Socket( props.getProperty(Savvy.PK_HOST), Integer.parseInt(props.getProperty(Savvy.PK_PORT)) );
            System.err.println("Communicator.connect: connected");
            out = new PrintWriter(sock.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
            connected = true;
            if( lis != null )
                synchronized( lis ) {
                    lis.notifyAll();  // wake up Listener
                }
        } catch (IOException e) {
            System.err.println("Communicator.connect: can't connect - waiting - retrying: " + e);
            try {
                sleep( 3*1000 );
            } catch (InterruptedException ex) { /*ignore*/ }
        }
    }

    private void connectionLost() {
        connected = false;
        System.err.println("Communicator: lost connection");
        interrupt();
    }
    
    private void lineReceived( String l ) {
        try {
            SMsg msg = Coder.parseServerMsg( l );
            if( msg.id != -1 ) {
                if( msg.id != lastId+1 )
                    System.err.println("error: msg id mismatch in receive, expected "+(lastId+1)+" but got "+msg.id+" - adjusting");
                lastId = msg.id;
            }
            Helper.putUntilPut( echo, msg );
            sess.receive( Coder.parseServerMsg(l) );
        } catch( IllegalArgumentException e ) {
                System.err.println("Communicator.lineReceived: ignoring: " + e);
        }
    }

    private final Session sess;
    private final Properties props;
    private Socket sock = null;
    private PrintWriter out = null;
    private BufferedReader in = null;
    private volatile int lastId = -1;
    private Listener lis;
    private final BlockingQueue<CMsg> toSend;
    private final BlockingQueue<SMsg> echo;
    private CMsg currMsg = null;
    private volatile boolean connected = false, running = false;

    private class Listener extends Thread {

        private Listener(Communicator c) {
            super("DoubleHead-Communicator-Listener");
            com = c;
        }

        @Override
        public void run() {
            do {
                do {
                    if( ! com.running )
                        break;
                    try {
                        synchronized (this) {
                            wait( 1 * 1000 );
                        }
                    } catch (InterruptedException e) {
                        System.err.println("Listener.run: wait interrupted");
                    }
                } while (!com.connected);
                while (com.connected) {
                    if( ! com.running )
                        break;
                    try {
                        String line = com.in.readLine();
                        if( line == null )
                            throw new IOException( "null read" );
//  don't show anymore                      System.out.println( "Com-Listener.run: received: "+(line.length()>14?line.substring(0,14):line)+"..." );
                        com.lineReceived(line);
                    } catch (IOException e) {
                        com.connectionLost();
                    }
                }
            } while( com.running );
            System.out.println( "Com-Listener.run: exiting " );
        }
        private Communicator com;
    }
}