/*
 * Copyright (C) 2020-2021 nessi, kaba
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.kaba.doublehead.middleware;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nessi, kaba
 */
public class PlayerMgr {
//    public enum Role { chefe, player, observer };

    PlayerMgr( Savvy m_frame, Session sess, String my_name ) {
        mFrame = m_frame;
        session = sess;
        myName = my_name;
        players = new ArrayList<>();
        onlPlayers = new ArrayList<>();
        nr1 = -1;
    }
    
    private List<Player> players;
    private List<Player> onlPlayers;
    private final Savvy mFrame;
    private final Session session;
    private final String myName;
    private int nr1=-1;
    private List<Player> active;

    public static class Player {
        private Player( String n, boolean m, boolean o ) { name=n; me=m; online=o; }
        public boolean getOnline() { return online; }
        @Override
        public String toString() { return "Player{" + name + "(" + (online?"on":"off") + ")}"; }
        public final String name;
        public final boolean me;
        protected boolean online;
    }

    synchronized boolean appeared( String name ) {
        Player p = findPlayer( name );
        if( p != null ) {
            if( p.online ) {
                System.err.println( "PlayerMgr.appeared: Error: player "+name+" already was online" );
                return false;
            } else {
                p.online = true;
                onlPlayers.add(p);
            }
        } else {  // p == null
            boolean m = false;
            if(name.equals(myName))
                m=true;
            Player np = new Player(name,m, true);
            players.add( np );
            onlPlayers.add(np);
            if( (players.size()==1) && (name.equals(myName)) )
                session.beChefe();
        }
        mFrame.showPlayers( players, 6 );
        return true;
    }

    synchronized boolean vanished( String name ) {
        Player p = findPlayer( name );
        if( p != null ) {
            if( p.online ) {
                onlPlayers.remove(p);
                p.online = false;
            }else {
                System.err.println( "PlayerMgr.vanished: Error: player "+name+" already was offline" );
                return false;
            }
        } else { // p == null
            System.err.println( "PlayerMgr.vanished: Error: player "+name+" is not listed" );
            return false;
        }
        mFrame.showPlayers( players, 6 );
        return true;
    }

    /** counts online players
     *
     * @return the number of online players
     */
    synchronized int count() {
        return onlPlayers.size();
    }

    void nextGame() {
        int before=nr1;
        nr1++;
        nr1 = nr1 % Math.min(6,count());
        assert nr1 < count();
        System.out.println( "next game: advancing start player from "+before+" to "+nr1+", having "+count()+" players: "+players );
        identifyActive();
    }

    private synchronized void identifyActive() {
        assert count() >= 4;
        active = new ArrayList<>( 4 );
        int p=nr1;
        for( int a=0; a<4; a++ ) {
            if( (a==2) && (count()>5) )
                p++; // skip opposite leper when 6
            p = p % Math.min(6,count());
            active.add( onlPlayers.get(p) );
            p++;
        }
        assert active.size() == 4;
        System.out.println( "identifyActive: players for next game: "+active );
    }

    List<Player> getActive() {
        assert active.size() == 4;
        return active;
    }

    private Player findPlayer( String name ) { // FIXME: not smart ...
        for( Player p : players )
            if( p.name.equals(name) )
                return p;
        return null;
    }
}
