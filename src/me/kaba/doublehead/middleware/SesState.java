/*
 * Copyright (C) 2020-2021 kaba
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.kaba.doublehead.middleware;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author kaba
 */
class SesState implements Cloneable {
    enum SupStates { wait4game, wait4cards, handsge4, inclmine, play, votewinner, votesge3ident, tricksge12 };
    enum SubStates { waitg, playersge4, waitnext, cardsge4, cardsent, canplay, none };
    static final Map<SubStates,SupStates> Sub2Sup = new HashMap<SubStates,SupStates>() {{
    put( SubStates.waitg, SupStates.wait4game );
    put( SubStates.playersge4, SupStates.wait4game );
    put( SubStates.waitnext, SupStates.wait4game );
    put( SubStates.cardsge4, SupStates.play );
    put( SubStates.cardsent, SupStates.play );
    put( SubStates.canplay, SupStates.play ); }};
    static final Map<SupStates,SubStates> DefaultSup = new EnumMap<SupStates,SubStates>(SupStates.class) {{
    put( SupStates.wait4game, SubStates.playersge4 );
    put( SupStates.wait4cards, SubStates.none );
    put( SupStates.handsge4, SubStates.none );
    put( SupStates.inclmine, SubStates.none );
    put( SupStates.play, SubStates.canplay );
    put( SupStates.votewinner, SubStates.none );
    put( SupStates.votesge3ident, SubStates.none );
    put( SupStates.tricksge12, SubStates.none ); }};
    /** "when" in name means: Session controls extra constraints */
    enum Actions { addplayer, testplayers, remplayer, sendngonce, initgame, counthands, testhands, testmyhand, guiplaycardonce, countcards, testcards, initvoting, guivoting, countvotes, testvotes, assigntrick, testtricks, cleartrick, clearflagsplus, clearguipc, showtricks, sendcard, sendvote, setobserving, keepcalm, dealonce, error };

    static class Action {
        Action( Actions a, SesState p, StateEvent e ) {
            actions=Arrays.asList(a); prev=p; event=e; errStr=null;
        }
        Action( List<Actions> as, SesState p, StateEvent e ) {
            actions=as; prev=p; event=e; errStr=null;
        }
        Action( SesState p, StateEvent e, String s ) {
            prev=p; actions=Arrays.asList(Actions.error); event=e; errStr=s;
        }
        final List<Actions> actions;
        final SesState prev;
        final StateEvent event;
        final String errStr;
    }
    SesState() {
        currentSup = SupStates.wait4game;
        currentSub = SubStates.waitg;
        clone = false;
    }

    synchronized Action happend( StateEvent e ) {
        assert( ! clone );
        switch( e.event ) {
            case received:
                return receivedMsg( e );
            case clicked:
                return userClicked( e );
            case tested:
                return tested( e );
        }
        return new Action( this.cloneS(), e, "ran out cases in SesState.happened for "+e.event+ " while in "+current2text() );
    }

    private synchronized Action receivedMsg( StateEvent e ) {
        assert e.prev == null;
        switch( e.sMsg.cmd ) {
            case Coder.CMD_NAME_APPEARED:
                return receivedNameAppeared( e );
            case Coder.CMD_NAME_VANISHED:
                return receivedNameVanished( e );
            case Coder.CMD_NEW_HAND:
                return receivedNewHand( e );
            case Coder.CMD_PLAY_CARD:
                return receivedPlayCard( e );
            case Coder.CMD_VOTE_TRICK:
                return receivedVoteTrick( e );
            case Coder.CMD_PING:
                return receivedPing( e );
            case Coder.CMD_NEXT_GAME:
                return receivedNextGame( e );
            default:
                System.err.println( "Warning: SesState: unknown msg "+e.sMsg.toDebugString()+" , while in "+current2text() );
        }
        // state invariant to other messages
        return new Action( Actions.keepcalm, this.cloneS(), e );
    }

    private synchronized Action receivedNameAppeared( StateEvent e ) {
        SesState prev = this.cloneS();
        switch( currentSub ) {
            case waitg:
                return new Action( changeState(SubStates.playersge4,e,Arrays.asList(Actions.addplayer)), prev, e );
        }
        return new Action( changeState(prev,e,Arrays.asList(Actions.addplayer)), prev, e );
    }

    private synchronized Action receivedNameVanished( StateEvent e ) {
        SesState prev = this.cloneS();
        switch( currentSub ) {
            case waitnext:
                return new Action( changeState(SubStates.playersge4,e,Arrays.asList(Actions.remplayer)), prev, e );
        }
        return new Action( changeState(prev,e,Arrays.asList(Actions.remplayer)), prev, e );
    }

    private synchronized Action receivedNewHand( StateEvent e ) {
        SesState prev = this.cloneS();
        switch( currentSup ) {
            case wait4cards:
                return new Action( changeState(SupStates.handsge4,e,Arrays.asList(Actions.counthands)), prev, e );
        }
        return new Action( this.cloneS(), e, "illegally received newHand while in "+current2text() );
    }

    private synchronized Action receivedPlayCard( StateEvent e ) {
        SesState prev = this.cloneS();
        switch( currentSup ) {
            case play:
                return new Action( changeState(SubStates.cardsge4,e,Arrays.asList(Actions.countcards)), prev, e );
        }
        return new Action( this.cloneS(), e, "illegally received playCard while in "+current2text() );
    }

    private synchronized Action receivedVoteTrick( StateEvent e ) {
        SesState prev = this.cloneS();
        switch( currentSup ) {
            case votewinner:
                return new Action( changeState(SupStates.votesge3ident,e,Arrays.asList(Actions.countvotes)), prev, e );
        }
        return new Action( changeState(prev,e,Arrays.asList(Actions.keepcalm)), prev, e );
    }

    private synchronized Action receivedNextGame( StateEvent e ) {
        SesState prev = this.cloneS();
        switch( currentSup ) {
            case wait4game:
                return new Action( changeState(SupStates.wait4cards,e,Arrays.asList(Actions.initgame)), prev, e );
        }
        return new Action( prev, e, "illegally received nextGame while in "+current2text() );
    }

    private synchronized Action receivedPing( StateEvent e ) {
        SesState prev = this.cloneS();
        List<Actions> acts;
        switch( currentSub ) {
            case waitg:
                return new Action( changeState(SubStates.playersge4,e,Arrays.asList()), prev, e );
        }
        switch( currentSup ) {
            case wait4cards:
                return new Action( changeState(SupStates.handsge4,e,Arrays.asList()), prev, e );
            case play:
                return new Action( changeState(SubStates.cardsge4,e,Arrays.asList()), prev, e );
            case votewinner:
                return new Action( changeState(SupStates.votesge3ident,e,Arrays.asList()), prev, e );
        }
        return new Action( changeState(prev,e,Arrays.asList(Actions.keepcalm)), prev, e );
    }

    private synchronized Action userClicked( StateEvent e ) {
        SesState prev = this.cloneS();
        List<Actions> acts;
        switch( e.click ) {
            case sendcard:
                switch( currentSub ) {
                    case canplay:
                        return new Action( changeState(SubStates.cardsent,e,Arrays.asList(Actions.sendcard)), prev, e );
                        //break;
                    default:
                        return new Action( prev, e, "click sendcard illegal while in "+current2text() );
                }
                //break;
            case sendvote:
                switch( currentSup ) {
                    case votewinner:
                        return new Action( changeState(prev,e,Arrays.asList(Actions.sendvote)), prev, e );
                        //break;
                    case play:
                    case wait4game:
                        System.out.println( "click sendvote ignored while in "+current2text()+", being late" );
                        return new Action( changeState(prev,e,Arrays.asList(Actions.keepcalm)), prev, e );
                    default:
                        return new Action( prev, e, "click sendvote illegal while in "+current2text() );
                }
                //break;
            case ping:
                return new Action( Actions.keepcalm, prev, e ); // dummy here; never called & already sent by session
                //break;
            default:
                return new Action( this.cloneS(), e, "userClicked receives unknown "+e.click+" while in "+current2text() );
        }
    }

    private synchronized Action tested( StateEvent e ) {
        SesState prev = this.cloneS();
        List<Actions> acts;
        switch( currentSub ) {
            case playersge4:
                if( e.answer )
                    return new Action( changeState(SubStates.waitnext,e,Arrays.asList(Actions.keepcalm)), prev, e );
                else
                    return new Action( changeState(SubStates.waitg,e,Arrays.asList(Actions.keepcalm)), prev, e );
                //break;
            case cardsge4:
                if( e.answer ) {
                    if( e.observer )
                        return new Action( changeState(SupStates.votewinner,e,Arrays.asList(Actions.initvoting)), prev, e );
                    else
                        return new Action( changeState(SupStates.votewinner,e,Arrays.asList(Actions.initvoting,Actions.guivoting)), prev, e );
                } else
                    return new Action( changeState(e.prev,e,Arrays.asList(Actions.keepcalm)), prev, e );
                //break;
        }
        switch( currentSup ) {
            case handsge4:
                if( e.answer )
                    return new Action( changeState(SupStates.inclmine,e,Arrays.asList()), prev, e );
                else
                    return new Action( changeState(SupStates.wait4cards,e,Arrays.asList(Actions.keepcalm)), prev, e );
                //break;
            case inclmine:
                if( e.answer )
                    return new Action( changeState(SubStates.canplay,e,Arrays.asList(Actions.clearguipc)), prev, e );
                else
                    return new Action( changeState(SubStates.canplay,e,Arrays.asList(Actions.setobserving,Actions.clearguipc)), prev, e );
                //break;
            case votesge3ident:
                if( e.answer )
                    return new Action( changeState(SupStates.tricksge12,e,Arrays.asList(Actions.assigntrick)), prev, e );
                else
                    return new Action( changeState(SupStates.votewinner,e,Arrays.asList(Actions.keepcalm)), prev, e );
                //break;
            case tricksge12:
                if( e.answer ) {
                    if( e.observer )
                        return new Action( changeState(SubStates.playersge4,e,Arrays.asList(Actions.clearflagsplus)), prev, e );
                    else
                        return new Action( changeState(SubStates.playersge4,e,Arrays.asList(Actions.showtricks,Actions.clearflagsplus)), prev, e );
                } else
                    return new Action( changeState(SubStates.canplay,e,Arrays.asList(Actions.cleartrick,Actions.clearguipc)), prev, e );
                //break;
        }
        return new Action( prev, e, "tested receives unexpected answer while in "+current2text() );
    }

    private SesState cloneS() {
        try {
            SesState c = (SesState)(this.clone());
            c.clone = true;
            return c;
        } catch( CloneNotSupportedException e ) {
            System.err.println( "CAN NEVER HAPPEN" );
            throw new RuntimeException( e );
        }
    }

    private List<Actions> changeState( SupStates nuevo, StateEvent e, List<Actions> transacts ) {
        String before = "changeState(sup): "+current2text()+" -> ";
        currentSup = nuevo; currentSub = DefaultSup.get(nuevo);
        List<Actions> acts = addEntryActions( transacts, e );
        System.out.println( before+current2text()+" - "+acts );
        return acts;
    }
    private List<Actions> changeState( SubStates nuevo, StateEvent e, List<Actions> transacts ) {
        String before = "changeState(sub): "+current2text()+" -> ";
        currentSup = Sub2Sup.get(nuevo); currentSub = nuevo;
        List<Actions> acts = addEntryActions( transacts, e );
        System.out.println( before+current2text()+" - "+acts );
        return acts;
    }
    private List<Actions> changeState( SesState nuevo, StateEvent e, List<Actions> transacts ) {
        String before = "changeState(cop): "+current2text()+" -> ";
        currentSup = nuevo.currentSup; currentSub = nuevo.currentSub;
        List<Actions> acts = addEntryActions( transacts, e );
        System.out.println( before+current2text()+" - "+acts );
        return acts;
    }
    private List<Actions> addEntryActions( List<Actions> oacts, StateEvent e ) {
        boolean catchUp = ( (e.sMsg==null) || (e.sMsg.amCatchingUp()) );  // undecided interpreteted as true here
        List<Actions> nacts = new ArrayList<>( oacts );
        switch( currentSup ) { // for clusters, every entry into substate would enter superstate again!
            case wait4cards:
                if( (!catchUp) && (e.chefe) )
                    nacts.add(Actions.dealonce );
                break;
            case handsge4:
                nacts.add( Actions.testhands );
                break;
            case inclmine:
                nacts.add( Actions.testmyhand );
                break;
            case votesge3ident:
                nacts.add( Actions.testvotes );
                break;
            case tricksge12:
                nacts.add( Actions.testtricks );
                break;
        }
        switch( currentSub ) {
            case playersge4:
                nacts.add( Actions.testplayers );
                break;
            case waitnext:
                if( (!catchUp) && (e.chefe) )
                    nacts.add(Actions.sendngonce );
                break;
            case canplay:
                if( (!catchUp) && (!e.observer) && (!nacts.contains(Actions.setobserving)) )
                    nacts.add(Actions.guiplaycardonce );
                break;
            case cardsge4:
                nacts.add( Actions.testcards );
                break;
        }
        return nacts;
    }

    private SupStates currentSup;
    private SubStates currentSub;
    private boolean clone = false;
    private String current2text() {
        if( currentSub == SubStates.none )
            return ""+currentSup;
        else
            return currentSup+"("+currentSub+")";
    }
}
