/*
 * Copyright (C) 2020-2021 kaba
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.kaba.doublehead.middleware;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** codes session communication; expand for more ...
 *
 * @author kaba
 */
abstract public class Coder {

    public static CMsg register( String client ) {
        return new CMsg( -2, CMD_REGISTRATION, client, true );
    }

    public static SMsg rejectName( String client ) {
        return new SMsg( "-", CMD_NAME_REJECT, client );
    }

    public static SMsg acceptName( String client ) {
        return new SMsg( "-", CMD_NAME_ACCEPT, client );
    }

    public static SMsg appeared( String client ) {
        return new SMsg( "-", CMD_NAME_APPEARED, client );
    }

    public static SMsg vanished( String client ) {
        return new SMsg( "-", CMD_NAME_VANISHED, client );
    }

    public static CMsg cmdNewHand( String player, List<String> hand ) {
        StringBuilder arg = new StringBuilder( player );
        for( String s : hand )
            arg.append( " " ).append( s );
        return new CMsg( -2, CMD_NEW_HAND, arg.toString() );
    }

    public static CMsg cmdPlayCard( String username, String cardhandle ) {
        return new CMsg( -2, CMD_PLAY_CARD, username+" "+cardhandle );
    }
        
    public static CMsg cmdVoteTrick( String username, String winnername, int votebase ) {
        return new CMsg( -2, CMD_VOTE_TRICK, username+" "+winnername+" "+CMsg.id2str(votebase) );
    }
    
    public static CMsg cmdPing() {
        return new CMsg( -2, CMD_PING, "" );
    }
    
    public static CMsg cmdNextGame() {
        return new CMsg( -2, CMD_NEXT_GAME, "" );
    }
    
    public static CMsg parseClientMsg( String s ) throws IllegalArgumentException {
        try {
            int i = s.indexOf( " " );
            if( i < 1 )
                throw new IllegalArgumentException( "cannot locate ID in '"+s+"'" );
            int mid = Integer.parseInt( s.substring(0, i), 36 );
            String r = s.substring( i+1 );
            i = r.indexOf( " " );
            if( i != 1 )
                throw new IllegalArgumentException( "cannot locate CMD in '"+s+"'" );
            return new CMsg( mid, r.substring(0,1), r.substring(2) );
        } catch( NumberFormatException e ) {
                throw new IllegalArgumentException(e);
        }
    }

    public static SMsg parseServerMsg( String l ) throws IllegalArgumentException {
        try {
            int i = l.indexOf( " " );
            if( i < 1 )
                throw new IllegalArgumentException( "cannot locate ID in '"+l+"'" );
            int mid = Integer.parseInt( l.substring(0, i), 36 );
            String subl = l.substring( i+1 );
            i = subl.indexOf( " " );
            if( i < 1 )
                throw new IllegalArgumentException( "cannot locate MAX in '"+l+"'" );
            int max = Integer.parseInt( subl.substring(0, i), 36 );
            subl = subl.substring( i+1 );
            i = subl.indexOf( " " );
            if( i < 1 )
                throw new IllegalArgumentException( "cannot locate SENDER in '"+l+"'" );
            String sndr = subl.substring( 0, i );
            subl = subl.substring( i+1 );
            i = subl.indexOf( " " );
            if( i != 1 )
                throw new IllegalArgumentException( "cannot locate CMD in '"+l+"'" );
            return new SMsg( sndr, mid, max, subl.substring(0,1), subl.substring(2) );
        } catch( NumberFormatException e ) {
            throw new IllegalArgumentException(e);
        }
    }

    public static final String CMD_REGISTRATION = "a";
    public static final String CMD_NAME_ACCEPT = "b";
    public static final String CMD_NAME_REJECT = "c";
    public static final String CMD_NAME_APPEARED = "d";
    public static final String CMD_NAME_VANISHED = "e";

    public static final String CMD_NEW_HAND = "A";
    public static final String CMD_PLAY_CARD = "B";
    public static final String CMD_VOTE_TRICK = "C";
    public static final String CMD_PING = "D";
    public static final String CMD_NEXT_GAME = "E";

    public static final Map<String,String> CMD_NAMES = createCmdNames();
    private static final Map<String,String> createCmdNames() {
        Map<String,String> m = new HashMap<>();
        m.put( "a", "REGISTRATION" );
        m.put( "b", "NAME_ACCEPT" );
        m.put( "c", "NAME_REJECT" );
        m.put( "d", "NAME_APPEARED" );
        m.put( "e", "NAME_VANISHED" );
        m.put( "A", "NEW_HAND" );
        m.put( "B", "PLAY_CARD" );
        m.put( "C", "VOTE_TRICK" );
        m.put( "D", "PING" );
        m.put( "E", "NEXT_GAME" );
        return m;
    }
}
